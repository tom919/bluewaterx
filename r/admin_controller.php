<?php
  include_once"m_base.php";


  /*---port---*/
  function tampil_port(){
    $table="port";
    $parameter="ORDER BY nama_port";
    $query=view($table,$parameter);
    return $query;
  }

  function tampil_port_list($posisi,$batas){
    $table="port";
    $parameter="ORDER BY nama_port LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;
  }

  function jumlah_port(){
    $table="port";
    $query=count_data($table);
    return $query;
  }

  function tambah_port($data){
    $table="port";
    $field="nama_port,wilayah";
    $val=$data;
    $parameter="($field) VALUES ($val)";
    $q=insert($table,$parameter);
    if(!$q==1){
      $n="20";
      return $n;
      }else {
        $n="10";
        return $n;
      }
  }

  function pilih_port($id){
    $table="port";
    $parameter="WHERE id_port='$id'";
    $query=view($table,$parameter);
    return $query;
  }

  function edit_port($id_port,$nama_port,$wilayah){
    $table="port";
      $set_cond="nama_port='$nama_port',wilayah='$wilayah'";
    $parameter="WHERE id_port='$id_port'";
    $query=update($table,$set_cond,$parameter);
    return $query;
  }

  function hapus_port($id){
    $table="port";
    $parameter="id_port='$id'";
    $query=delete($table,$parameter);
    return $query;
  }

  function cari_port($term,$posisi,$batas){
    $table="port";
    $parameter="WHERE nama_port LIKE '%$term%' OR wilayah LIKE '%$term%' LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;
  }
  function modul_cari_port(){
    echo"    <form method='get' action='search_port'>
        <div class='input-group input-group-sm' style='width: 200px;''>


          <input type='text' name='term' class='form-control' placeholder='Cari Port' required>

          <div class='input-group-btn'>
            <button type='submit' class='btn btn-default'><i class='fa fa-search'></i></button>
          </div>
        </div>
      </form>";
  }

  /*--- perusahaan --*/
  function tambah_perusahaan($data){
    $table="perusahaan";
    $field="id_per,nama_perusahaan,alamat,telp,email";
    $val=$data;
    $parameter="($field) VALUES ($val)";
    $q=insert($table,$parameter);
    if(!$q==1){
      $n="20";
      return $n;
      }else {
        $n="10";
        return $n;
      }
  }
  function tampil_perusahaan($posisi, $batas){
    $table="perusahaan";

    $parameter="ORDER BY id_per ASC LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;

  }

  function cari_perusahaan($term,$posisi,$batas){
    $table="perusahaan";
    $parameter="WHERE id_per LIKE '%$term%' OR nama_perusahaan LIKE '%$term%' OR alamat LIKE '%$term%' OR telp LIKE '%$term%' OR email LIKE '%$term%' LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;
  }

  function modul_cari_perusahaan(){
    echo"    <form method='get' action='search_company'>
        <div class='input-group input-group-sm' style='width: 200px;''>


          <input type='text' name='term' class='form-control' placeholder='Cari Perusahaan' required>

          <div class='input-group-btn'>
            <button type='submit' class='btn btn-default'><i class='fa fa-search'></i></button>
          </div>
        </div>
      </form>";
  }

  function pilih_perusahaan($id){
    $table="perusahaan";
    $parameter="WHERE id_per='$id'";
    $query=view($table,$parameter);
    return $query;
  }

  function edit_perusahaan($id_per,$nama_perusahaan,$alamat,$telp,$email){
    $table="perusahaan";
      $set_cond="nama_perusahaan='$nama_perusahaan',alamat='$alamat',telp='$telp',email='$email'";
    $parameter="WHERE id_per='$id_per'";
    $query=update($table,$set_cond,$parameter);
    return $query;
  }

  function hapus_perusahaan($id){
    $table="perusahaan";
    $parameter="id_per='$id'";
    $query=delete($table,$parameter);
    return $query;
  }
  function jumlah_perusahaan(){
    $table="perusahaan";
    $query=count_data($table);
    return $query;

  }

  /*--- BOAT --*/
  function tambah_boat($data){
    $table="boat";
    $field="id_boat,nama_boat,kapasitas,ref_per";
    $val=$data;
    $parameter="($field) VALUES ($val)";
    $q=insert($table,$parameter);
    if(!$q==1){
      $n="20";
      return $n;
      }else {
        $n="10";
        return $n;
      }
  }
  function tampil_boat($posisi, $batas){
    $table="boat";

    $parameter="ORDER BY id_boat ASC LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;

  }

  function cari_boat($term,$posisi,$batas){
    $table="boat";
    $parameter="WHERE id_boat LIKE '%$term%' OR nama_boat LIKE '%$term%' OR kapasitas LIKE '%$term%' OR ref_per LIKE '%$term%' LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;
  }

  function modul_cari_boat(){
    echo"    <form method='get' action='search_boat'>
        <div class='input-group input-group-sm' style='width: 200px;''>


          <input type='text' name='term' class='form-control' placeholder='Cari Boat' required>

          <div class='input-group-btn'>
            <button type='submit' class='btn btn-default'><i class='fa fa-search'></i></button>
          </div>
        </div>
      </form>";
  }

  function pilih_boat($id){
    $table="boat";
    $parameter="WHERE id_boat='$id'";
    $query=view($table,$parameter);
    return $query;
  }

  function edit_boat($id_boat,$nama_boat,$kapasitas,$ref_per){
    $table="boat";
      $set_cond="nama_boat='$nama_boat',kapasitas='$kapasitas',ref_per='$ref_per'";
    $parameter="WHERE id_boat='$id_boat'";
    $query=update($table,$set_cond,$parameter);
    return $query;
  }

  function hapus_boat($id){
    $table="boat";
    $parameter="id_boat='$id'";
    $query=delete($table,$parameter);
    return $query;
  }
  function jumlah_boat(){
    $table="boat";
    $query=count_data($table);
    return $query;
  }
  function referensi_per(){
    $table="perusahaan";
    $parameter="ORDER BY id_per ASC";
      $query=view($table,$parameter);
      return $query;
  }
  function cari_ref_per($term){
    $table="perusahaan";
    $parameter="WHERE id_per ='$term'";
    $query=view($table,$parameter);
    return $query;
  }

  /*---- jadwal ----*/
  function tambah_jadwal($data){
    $table="jadwal";
    $field="id_kapal,keberangkatan,kedatangan,port_keberangkatan,port_kedatangan,jenis,tarif_dewasa,tarif_anak";
    $val=$data;
    $parameter="($field) VALUES ($val)";
    $q=insert($table,$parameter);
    if(!$q==1){
      $n="20";
      return $n;
      }else {
        $n="10";
        return $n;
      }
  }
  function tampil_jadwal($posisi, $batas){
    $table="jadwal";

    $parameter="ORDER BY id ASC LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;

  }

  function cari_jadwal($term,$posisi,$batas){
    $table="jadwal";
    $parameter="WHERE id LIKE '%$term%' OR id_kapal LIKE '%$term%' OR keberangkatan LIKE '%$term%' OR kedatangan LIKE '%$term%' OR port_keberangkatan LIKE '%$term%' OR port_kedatangan LIKE '%$term%' OR jenis LIKE '%$term%' OR tarif_dewasa LIKE '%$term%' tarif_anak LIKE '%$term%' LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;
  }

  function modul_cari_jadwal(){
    echo"    <form method='get' action='search_schedule'>
        <div class='input-group input-group-sm' style='width: 200px;''>


          <input type='text' name='term' class='form-control' placeholder='Cari Jadwal' required>

          <div class='input-group-btn'>
            <button type='submit' class='btn btn-default'><i class='fa fa-search'></i></button>
          </div>
        </div>
      </form>";
  }

  function pilih_jadwal($id){
    $table="jadwal";
    $parameter="WHERE id='$id'";
    $query=view($table,$parameter);
    return $query;
  }

  function edit_jadwal($id,$id_kapal,$keberangkatan,$kedatangan,$port_keberangkatan,$port_kedatangan,$jenis,$tarif_dewasa,$tarif_anak){
    $table="jadwal";
      $set_cond="id_kapal='$id_kapal',keberangkatan='$keberangkatan',kedatangan='$kedatangan',port_keberangkatan='$port_keberangkatan',port_kedatangan='$port_kedatangan',jenis='$jenis',tarif_dewasa='$tarif_dewasa',tarif_anak='$tarif_anak'";
    $parameter="WHERE id='$id'";
    $query=update($table,$set_cond,$parameter);
    return $query;
  }

  function hapus_jadwal($id){
    $table="jadwal";
    $parameter="id='$id'";
    $query=delete($table,$parameter);
    return $query;
  }
  function jumlah_jadwal(){
    $table="jadwal";
    $query=count_data($table);
    return $query;
  }
  function tampil_all_ref_boat(){
    $table="boat";
    $parameter="";
    $query=view($table,$parameter);
    return $query;
  }
  function tampil_ref_boat($id){
    $table="boat";
    $parameter="WHERE id_boat='$id'";
    $query=view($table,$parameter);
    return $query;
  }

/*--- booking---*/

function tampil_booking($posisi, $batas){

  $parameter="SELECT booking.id_booking id_booking, booking.kode_booking kode_booking, booking.kode_unik kode_unik, booking.tanggal tanggal, booking.tanggal_berangkat tanggal_berangkat, booking.status status, customer.id_customer id_customer,customer.nama_customer nama_customer FROM booking INNER JOIN customer ON booking.id_customer=customer.id_customer LIMIT $posisi,$batas";
  $query=base_view($parameter);
  return $query;

}

function jumlah_booking(){
  $table="booking";
  $query=count_data($table);
  return $query;
}
function tampil_booking_terbaru(){
  $table="booking";
  $parameter="ORDER BY tanggal DESC LIMIT 10";
  $query=view($table,$parameter);
  return $query;
}

function tampil_ref_customer($id_customer){
  $table="customer";
  $parameter="WHERE id_customer='$id_customer' ";
  $query=view($table,$parameter);
  return $query;
}

  function hitung_booking_terbaru()
  {
    $table="booking";
    $parameter="ORDER BY tanggal DESC LIMIT 10";
    $query=count_data_spec($table,$parameter);
    return $query;
  }

  function hitung_booking_unconfirmed()
  {
    $table="booking";
    $parameter="WHERE status='unconfirmed' ";
    $query=count_data_spec($table,$parameter);
    return $query;
  }

  function hitung_booking_confirmed()
  {
    $table="booking";
    $parameter="WHERE status='paid' ";
    $query=count_data_spec($table,$parameter);
    return $query;
  }

  function modul_cari_booking(){
    echo"    <form method='get' action='search_booking'>

    <select class='form-control' name='fields' required>
    <option selected value='booking.kode_booking'>Kode Booking</option>
    <option value='booking.kode_unik'>Kode Unik</option>
    <option value='booking.status'>Status</option>
    <option value='customer.nama_customer'>Nama Customer</option>
    <option value='customer.alamat'>Alamat</option>
    <option value='customer.kota'>Kota</option>
    <option value='customer.negara'>Negara</option>
    <option value='customer.telp'>Telp</option>
    <option value='customer.email'>Email</option>
    <option value='pembayaran.payment_type'>Jenis Pembayaran</option>
    <option value='pembayaran.payment_acc'>No Akun</option>
    <option value='booking.tanggal'>Tanggal Booking</option>
    <option value='booking.tanggal_berangkat'>Tanggal Keberangkatan</option>
    <option value='booking.opsi_pickup'>Opsi Pickup</option>
    <option value='booking.alamat_pickup'>Alamat pickup</option>
    <option value='penumpang.nama_penumpang'>Nama Penumpang</option>
    <option value='penumpang.umur'>Umur Penumpang</option>
    <option value='penumpang.jenis_kelamin'>Jenis Kelamin</option>
    </select>
    <br>
        <div class='input-group input-group-sm' style='width: 200px;''>


          <input type='text' name='term' class='form-control' placeholder='Cari' required>



          <div class='input-group-btn'>
            <button type='submit' class='btn btn-default'><i class='fa fa-search'></i></button>
          </div>
        </div>
      </form>";
  }

  function cari_booking_general($term,$fields,$posisi,$batas){

    $parameter="SELECT booking.id_booking id_booking, booking.kode_booking kode_booking, booking.kode_unik kode_unik, booking.tanggal tanggal, booking.tanggal_berangkat tanggal_berangkat, booking.status status, customer.id_customer id_customer, customer.nama_customer nama_customer FROM booking INNER JOIN customer ON booking.id_customer=customer.id_customer AND $fields LIKE '%$term%' LIMIT $posisi,$batas";
    $query=base_view($parameter);
    return $query;
  }



  function pilih_booking($id){
    $parameter="SELECT booking.id_booking id_booking, booking.kode_booking kode_booking, booking.kode_unik kode_unik, booking.tanggal tanggal, booking.tanggal_berangkat tanggal_berangkat, booking.opsi_pickup opsi_pickup, booking.alamat_pickup alamat_pickup, booking.status status,customer.nama_customer nama_customer, customer.negara negara,  jadwal.port_keberangkatan port_keberangkatan, jadwal.port_kedatangan port_kedatangan, pembayaran.payment_type payment_type, pembayaran.payment_acc payment_acc FROM booking INNER JOIN customer INNER JOIN jadwal INNER JOIN pembayaran ON booking.id_customer=customer.id_customer AND booking.id_jadwal=jadwal.id AND booking.id_booking=pembayaran.payment_cust_id AND booking.id_booking='$id'";
    $query=base_view($parameter);
    return $query;
  }


  function pilih_booking_tiket($id){
    $parameter="SELECT booking.id_booking id_booking, booking.kode_booking kode_booking, booking.kode_unik kode_unik, booking.tanggal_berangkat tanggal_berangkat, booking.status status,customer.nama_customer nama_customer, customer.email email, jadwal.keberangkatan jam_keberangkatan, jadwal.kedatangan jam_kedatangan,jadwal.port_keberangkatan port_keberangkatan, jadwal.port_kedatangan port_kedatangan FROM booking INNER JOIN customer INNER JOIN jadwal ON booking.id_customer=customer.id_customer AND booking.id_jadwal=jadwal.id AND booking.id_booking='$id'";
    $query=base_view($parameter);
    return $query;
  }

  function tampil_penumpang($id){
    $table="penumpang";
    $parameter="WHERE id_booking='$id' ";
    $query=view($table,$parameter);
    return $query;
  }
  function tampil_pembayaran($id){
    $table="pembayaran";
    $parameter="WHERE payment_cust_id='$id' ";
    $query=view($table,$parameter);
    return $query;
  }

  function edit_booking($id,$status){
    $table="booking";
      $set_cond="status='$status'";
    $parameter="WHERE id_booking='$id'";
    $query=update($table,$set_cond,$parameter);
    return $query;
  }

  function hapus_booking($id,$idc){
    $table="booking";
    $table2="customer";
    $parameter="id_booking='$id'";
    $parameter2="id_customer='$idc'";
    $query=delete($table,$parameter);
    $query2=delete($table2,$parameter2);
    return $query;
  }

  /*--- REST SERVER --*/
/*  function validasi($idc)
  {

    return $result;
  }--*/


  /*-- profil --*/
  function update_password($id,$password){
    $table="user";
    $pass1=hash('sha256',$password);
      $set_cond="password='$pass1'";
    $parameter="WHERE username='$id'";
    $query=update($table,$set_cond,$parameter);
    return $query;
  }
  /*--- slider --*/

  function tambah_slide($data){
    $table="slider";
    $field="foto,caption,content,link_title,link";
    $val=$data;
    $parameter="($field) VALUES ($val)";
    $q=insert($table,$parameter);
    if(!$q==1){
      $n="20";
      return $n;
      }else {
        $n="10";
        return $n;
      }
  }
  function edit_slide($id,$set_cond){
    $table="slider";

    $parameter="WHERE id='$id'";
    $query=update($table,$set_cond,$parameter);
    return $query;
  }

  function tampil_slider($posisi, $batas){
    $table="slider";

    $parameter="LIMIT $posisi,$batas";
    $query=view($table,$parameter);
    return $query;

  }


  function jumlah_slide(){
    $table="slider";
    $query=count_data($table);
    return $query;
  }

  function pilih_slider($id){
    $table="slider";
    $parameter="WHERE id='$id'";
    $query=view($table,$parameter);
    return $query;
  }
  function hapus_slide($id){
    $table="slider";
    $parameter="id='$id'";
    $query=delete($table,$parameter);
    return $query;
  }

?>
