// GOOGLE MAP - GREYSCALE
google.maps.event.addDomListener(window, 'load', init);

function init() {
	var mapOptions = {
	
		zoom: 9,

		center: new google.maps.LatLng(-8.510398,114.9125716), // New York

		styles: [{
			"featureType": "landscape",
			"stylers": [{
				"visibility": "on"
			}]
		}, {
			"featureType": "poi",
			"stylers": [{
				"visibility": "simplified"
			}]
		}, {
			"featureType": "road.highway",
			"stylers": [{
				"visibility": "simplified"
			}]
		}, {
			"featureType": "road.arterial",
			"stylers": [{
				"visibility": "on"
			}]
		}, {
			"featureType": "road.local",
			"stylers": [{
				"visibility": "on"
			}]
		}, {
			"featureType": "transit",
			"stylers": [{
				"visibility": "simplified"
			}]
		}, {
			"featureType": "administrative.province",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "water",
			"elementType": "labels",
			"stylers": [{
				"visibility": "on"
			},]
		}, {
			"featureType": "water",
			"elementType": "geometry",
			"stylers": []
		}]
	};

	var mapElement = document.getElementById('map-greyscale');

	var map = new google.maps.Map(mapElement, mapOptions);

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(-8.6725072, 115.1542214),
		map: map,
		title: 'bali'
	});
}
