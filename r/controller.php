<?php
include_once"m_base.php";
include_once"setting.php";
/*---port---*/
function tampil_port(){
  $table="port";
  $parameter="ORDER BY nama_port";
  $query=view($table,$parameter);
  return $query;
}

/*---- avability ----*/


function search_avability($departure_place,$arrival_place){
  $table="jadwal";
  $parameter="WHERE port_keberangkatan='$departure_place' AND port_kedatangan='$arrival_place'";
  $query=view($table,$parameter);
  return $query;
}

function check_capacity($id)
{
  $table="boat";
  $parameter="WHERE id_boat='$id'";
  $query=view($table,$parameter);
  return $query;
}

function check_onboard_amount($id_jadwal,$date){
  $parameter="SELECT penumpang.nama_penumpang nama_penumpang FROM booking INNER JOIN penumpang INNER JOIN jadwal ON booking.id_jadwal=jadwal.id AND penumpang.id_booking=booking.id_booking AND jadwal.id='$id_jadwal' AND booking.tanggal_berangkat='$date'";
  $query=base_view($parameter);
  return $query;
}

/*----- ref boat ----*/
function tampil_ref_boat($id){
  $table="boat";
  $parameter="WHERE id_boat='$id'";
  $query=view($table,$parameter);
  return $query;
}

function tampil_ref_boat_jadwal($id_jadwal){
  $sub_table="jadwal";
  $table="boat";
  $parameter=" WHERE id_boat = (SELECT id_kapal FROM $sub_table WHERE id = '$id_jadwal')";
  $query=view($table,$parameter);
  return $query;
}

 /*---pembayaran---*/
 function pilih_pembayaran($opsi_bayar){
    switch($opsi_bayar){
      case "bank_transfer":
        echo"Bank Transfer";
        break;
      case "paypal" :
        echo "Paypal";
        break;
      case "doku" :
        echo "doku";
        break;
      case "fasapay" :
        echo "fasapay";
        break;
    }
 }

 function tampil_harga($trip){
   $table="jadwal";
   $parameter="WHERE id='$trip'";
   $query=view($table,$parameter);
   return $query;
 }

function pickup_status($cust_pickup){
  if($cust_pickup=="1"){
    echo"Yes";
  }
  else{
    echo"No";
  }
}


 function kode_booking(){
   $s1=date("ymdH");
   $p1=rand(60,99);
   $s=$s1.$p1;
   return "#".$s;
 }
 function kode_unik(){
   $p1=rand(10,20);
   $p2=rand(90,99);
   $p3=rand(30,40);
   $p4=rand(50,60);
   $n=2;
   $aKod = NULL;
     $kode = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for ($i=0; $i<$n; $i++) {
     $acakAngka = rand(1, strlen($kode));
     $aKod .= substr($kode, $acakAngka, 1);
  }
  for ($i=0; $i<$n; $i++) {
     $acakAngka = rand(1, strlen($kode));
     $aKod2 .= substr($kode, $acakAngka, 1);
  }

   $kode=$p1.$aKod.$p4.$aKod2;
   return $kode;
 }
/*--- pembayaran ---*/
function pembayaran($method_bayar,$booking_code,$grandtotal){
  switch ($method_bayar) {
    case 'banktransfer':
      $string="Please Transfer &nbsp; Rp. &nbsp;".$grandtotal." to our Bank Account $bank_acc_config<br>".
      "Insert your booking ID, at Your Transfer Notice";
      return $string;
      break;

      case 'paypal':

      $paypal_url=$paypal_url_config; // Test Paypal API URL
      $paypal_id=$paypal_id_email; // Business email ID

      $grandtotal_usd=$grandtotal/$global_kurs;
        $string="<br>USD/IDR Rate : $global_kurs".
        "<br>Total Payment USD $grandtotal_usd".
        "<br><div class='btn'>
          <form action='$paypal_url' method='post' name='frmPayPal1'>
          <input type='hidden' name='business' value='$paypal_id'>
          <input type='hidden' name='cmd' value='_xclick'>
          <input type='hidden' name='item_name' value='Bluewater-x Payment'>
          <input type='hidden' name='item_number' value='$booking_code'>

          <input type='hidden' name='userid' value='1'>
          <input type='hidden' name='amount' value='$grandtotal_usd'>
          <input type='hidden' name='no_shipping' value='1'>
          <input type='hidden' name='currency_code' value='USD'>
          <input type='hidden' name='handling' value='0'>
          <input type='hidden' name='cancel_return' value='paypall_cancel.php'>
          <input type='hidden' name='return' value='localhost://bluewaterx/v/paypall_success.php'>
          <input type='image' src='https://www.sandbox.paypal.com/en_US/i/btn/btn_paynowCC_LG.gif' border='0' name='submit' alt='PayPal - The safer, easier way to pay online!'>
          <img alt='' border='0' src='https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif' width='1' height='1'>
          </form>
          </div>
      </div>";
      return $string;
        break;

    default:
      # code...
      break;
  }

}




/*---input customer--*/
function input_customer($val){
  /*--- insert customer---*/
    $table_cust="customer";
    $parameter_cust="(nama_customer,alamat,kota,negara,telp,email) VALUES ($val)";
    $qc=insert($table_cust,$parameter_cust);

    	if($qc)
    	{
        return 10;

    	}
    	else
    	{
    		return 20;


    	}


}

function select_customer($parameter1,$parameter2){
  $table="customer";
  $parameter="WHERE nama_customer='$parameter1' AND email='$parameter2'";
  $query=view($table,$parameter);
  return $query;
}



/*----input booking--- */
function input_booking($book_val){
  $table_cust="booking";
  $parameter_cust="(kode_booking,kode_unik,id_customer,id_jadwal,tanggal,tanggal_berangkat,opsi_pickup,alamat_pickup,status) VALUES ($book_val)";
  $qq=insert($table_cust,$parameter_cust);

    if($qq)
    {
      return 10;

    }
    else
    {
      return 20;
    }

}


function select_booking($parameter1,$parameter2){
  $table="booking";
  $parameter="WHERE kode_unik='$parameter1' AND tanggal='$parameter2'";
  $query=view($table,$parameter);
  return $query;
}

/*--- input pembayaran--*/
function input_pembayaran($payment_val){
  $table_payment="pembayaran";
  $parameter_payment="(payment_type,payment_acc,adult_amount,child_amount,adult_ticket_price,child_ticket_price,payment_amount,payment_cust_id,payment_status) VALUES ($payment_val)";
  $qz=insert($table_payment,$parameter_payment);

    if($qz)
    {
      return 10;

    }
    else
    {
      return 20;
    }
}

/*--- input penumpang--*/
function input_penumpang($p_val){
  $table_payment="penumpang";
  $parameter_payment="(nama_penumpang,umur,jenis_kelamin,id_booking) VALUES ($p_val)";
  $qz=insert($table_payment,$parameter_payment);

    if($qz)
    {
      return 10;

    }
    else
    {
      return 20;
    }
}
/*--- kirim email booking ---*/
function kirim_email_booking($to,$subject,$message){
  $header = "MIME-Version: 1.0" . "\r\n";
$header .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
$header .= 'From: '.$booking_email. "\r\n";
$header .= 'Cc: ' . "\r\n";
    $sent=mail($to,$subject,$message,$header);
    if($sent){
      echo"";
    }
    else{
      echo "send mail fail";
    }
}

/*-- slider -- */
function tampil_slider($posisi, $batas){
  $table="slider";

  $parameter="LIMIT $posisi,$batas";
  $query=view($table,$parameter);
  return $query;

}



 ?>
