-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2017 at 02:39 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bluewater`
--

-- --------------------------------------------------------

--
-- Table structure for table `boat`
--

CREATE TABLE IF NOT EXISTS `boat` (
  `id_boat` int(20) NOT NULL DEFAULT '0',
  `nama_boat` varchar(255) NOT NULL,
  `kapasitas` int(10) NOT NULL,
  `ref_per` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boat`
--

INSERT INTO `boat` (`id_boat`, `nama_boat`, `kapasitas`, `ref_per`) VALUES
(1, 'Starfish Patrick', 25, 100),
(2, 'Black Pearl', 15, 101),
(3, 'Starfish Crab', 20, 100),
(4, 'Fifa la france', 30, 104),
(5, 'Kursk', 50, 102),
(6, 'Vladimir Svedrlov', 20, 102);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
`id_booking` int(255) NOT NULL,
  `kode_booking` varchar(255) NOT NULL,
  `kode_unik` varchar(255) NOT NULL,
  `id_customer` int(255) NOT NULL,
  `id_jadwal` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `tanggal_berangkat` date NOT NULL,
  `opsi_pickup` varchar(3) NOT NULL,
  `alamat_pickup` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id_booking`, `kode_booking`, `kode_unik`, `id_customer`, `id_jadwal`, `tanggal`, `tanggal_berangkat`, `opsi_pickup`, `alamat_pickup`, `status`) VALUES
(12, '#1703070895', '18LM58JP', 11, 4, '2017-03-07', '2017-03-20', '1', 'Ubud', 'paid'),
(15, '#1703091674', '15D54NZ', 13, 5, '2017-03-09', '2017-03-24', '1', 'Denpasar', 'unconfirmed'),
(16, '#1703091674', '13KN57NS', 13, 7, '2017-03-09', '2017-03-26', '1', 'Gili Air', 'unconfirmed'),
(17, '#1703091775', '11QR58K', 14, 4, '2017-03-09', '2017-03-22', '1', 'Sanur', 'unconfirmed'),
(18, '#1703091871', '16WG59JF', 15, 4, '2017-03-09', '2017-03-24', '1', 'Denpasar', 'unconfirmed'),
(19, '#1703091871', '11XN51QU', 15, 6, '2017-03-09', '2017-03-26', '1', 'Gili Trawangan', 'unconfirmed'),
(20, '#1703091980', '20OF57OW', 16, 4, '2017-03-09', '2017-03-24', '1', 'uluwatu', 'paid'),
(21, '#1703091980', '14KJ60NL', 16, 6, '2017-03-09', '2017-03-26', '1', 'gili trawangan', 'paid'),
(22, '#1703091964', '13OY56SN', 17, 4, '2017-03-09', '2017-03-24', '1', 'Sanur', 'unconfirmed'),
(23, '#1703091964', '20HU57YQ', 17, 6, '2017-03-09', '2017-03-26', '1', 'Gili Trawangan', 'unconfirmed'),
(24, '#1703101090', '19WY51ZE', 18, 4, '2017-03-10', '2017-03-24', '1', 'denpasar', 'unconfirmed'),
(25, '#1703101090', '16NX54PF', 18, 6, '2017-03-10', '2017-03-26', '1', 'gili trawangan', 'unconfirmed');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`id_customer` int(255) NOT NULL,
  `nama_customer` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `negara` varchar(100) NOT NULL,
  `telp` varchar(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `alamat`, `kota`, `negara`, `telp`, `email`) VALUES
(11, 'jean', 'lafayette ave 12', 'paris', 'france', '098234375', 'professional.it919@gmail.com'),
(13, 'John', 'St Regis Ave 20th', 'Marseille', 'France', '78468764973', 'sandiyudha919@yahoo.co.id'),
(14, 'Steve', 'Bienvenue 201', 'Berlin', 'Germany', '94753902', 'sandiyudha919@yahoo.co.id'),
(15, 'Greg', 'Astro 117', 'London', 'United Kingdom', '874365465', 'sandiyudha919@yahoo.co.id'),
(16, 'Enrique', 'bailando 56', 'madrid', 'spain', '76576476', 'sandiyudha919@yahoo.co.id'),
(17, 'Abu Sayaf', 'al maidah 55', 'dubai', 'Emirate United ', '9999999', 'sandiyudha919@yahoo.co.id'),
(18, 'Jimmy', 'northen 12', 'ontario', 'canada', '34768576465', 'sandiyudha919@yahoo.co.id');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE IF NOT EXISTS `jadwal` (
`id` int(11) NOT NULL,
  `id_kapal` int(20) DEFAULT NULL,
  `keberangkatan` time NOT NULL,
  `kedatangan` time NOT NULL,
  `port_keberangkatan` varchar(100) NOT NULL,
  `port_kedatangan` varchar(100) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `tarif_dewasa` double NOT NULL,
  `tarif_anak` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id`, `id_kapal`, `keberangkatan`, `kedatangan`, `port_keberangkatan`, `port_kedatangan`, `jenis`, `tarif_dewasa`, `tarif_anak`) VALUES
(2, 2, '10:00:00', '15:10:00', 'Padang bai', 'Gili Air', 'regular', 70000, 35000),
(3, 4, '09:00:00', '12:30:00', 'Padang bai', 'Teluk Kodek', 'regular', 66000, 0),
(4, 6, '10:15:00', '12:30:00', 'Serangan', 'Gili Trawangan', 'regular', 60000, 30000),
(5, 5, '05:00:00', '06:10:00', 'Serangan', 'Gili Air', 'regular', 60000, 15000),
(6, 3, '16:05:00', '18:06:00', 'Gili Trawangan', 'Serangan', 'regular', 60000, 20000),
(7, 4, '19:00:00', '21:00:00', 'Gili Air', 'Serangan', 'regular', 60000, 0),
(8, 1, '07:25:00', '10:30:00', 'Padang Bai', 'Gili Air', 'regular', 60000, 0),
(9, 3, '18:14:00', '22:00:00', 'Gili Air', 'Padang Bai', 'regular', 60000, 0),
(10, 6, '21:22:33', '23:22:37', 'Gili Trawangan', 'Gili Air', 'regular', 30000, 15000);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE IF NOT EXISTS `pembayaran` (
`payment_id` bigint(20) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `payment_acc` varchar(200) NOT NULL,
  `adult_amount` int(10) NOT NULL,
  `child_amount` int(10) NOT NULL,
  `adult_ticket_price` double NOT NULL,
  `child_ticket_price` double NOT NULL,
  `payment_amount` double NOT NULL,
  `payment_cust_id` int(255) NOT NULL,
  `payment_status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`payment_id`, `payment_type`, `payment_acc`, `adult_amount`, `child_amount`, `adult_ticket_price`, `child_ticket_price`, `payment_amount`, `payment_cust_id`, `payment_status`) VALUES
(10, 'paypal', 'professional.it919@gmail.com', 2, 0, 60000, 30000, 120000, 12, 'unpaid'),
(13, 'banktransfer', '927366822', 2, 2, 60000, 0, 120000, 15, 'unpaid'),
(14, 'banktransfer', '927366822', 2, 2, 60000, 0, 120000, 16, 'unpaid'),
(15, 'banktransfer', '2435981', 1, 0, 60000, 30000, 60000, 17, 'unpaid'),
(16, 'paypal', 'sandiyudha919@yahoo.co.id', 2, 0, 60000, 30000, 120000, 18, 'unpaid'),
(17, 'paypal', 'sandiyudha919@yahoo.co.id', 2, 0, 60000, 20000, 120000, 19, 'unpaid'),
(18, 'paypal', 'sandiyudha919@yahoo.co.id', 2, 1, 60000, 30000, 150000, 20, 'unpaid'),
(19, 'paypal', 'sandiyudha919@yahoo.co.id', 2, 1, 60000, 20000, 140000, 21, 'unpaid'),
(20, 'banktransfer', '345673546', 3, 0, 60000, 30000, 180000, 22, 'unpaid'),
(21, 'banktransfer', '345673546', 3, 0, 60000, 20000, 180000, 23, 'unpaid'),
(22, 'paypal', 'sandiyudha919@yahoo.co.id', 2, 1, 60000, 30000, 150000, 24, 'unpaid'),
(23, 'paypal', 'sandiyudha919@yahoo.co.id', 2, 1, 60000, 20000, 140000, 25, 'unpaid');

-- --------------------------------------------------------

--
-- Table structure for table `penumpang`
--

CREATE TABLE IF NOT EXISTS `penumpang` (
`id_penumpang` int(255) NOT NULL,
  `nama_penumpang` varchar(255) NOT NULL,
  `umur` varchar(5) NOT NULL,
  `jenis_kelamin` varchar(6) NOT NULL,
  `id_booking` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penumpang`
--

INSERT INTO `penumpang` (`id_penumpang`, `nama_penumpang`, `umur`, `jenis_kelamin`, `id_booking`) VALUES
(4, 'Jean', '28', 'Female', 12),
(5, 'Frank', '29', 'Male', 12),
(9, 'John', '28', 'Male', 15),
(10, 'Marie Antoniette', '27', 'Female', 15),
(11, 'Merlin Beard', '8', 'Male', 15),
(12, 'Arthur Butt', '5', 'Male', 15),
(13, 'Steve', '24', 'Male', 17),
(14, 'Greg', '40', 'Male', 18),
(15, 'Anie', '35', 'Female', 18),
(16, 'enrique', '30', 'Male', 20),
(17, 'ana karla suarez', '22', 'Female', 20),
(18, 'sean iglesias', '14', 'Male', 20),
(19, 'Abu sayaf', '70', 'Male', 22),
(20, 'Abu Gosok', '75', 'Male', 22),
(21, 'Abu Salman', '80', 'Male', 22),
(22, 'Jimmy', '22', 'Male', 24),
(23, 'Jinx', '21', 'Female', 24),
(24, 'Donald', '2', 'Male', 24);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE IF NOT EXISTS `perusahaan` (
  `id_per` int(100) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id_per`, `nama_perusahaan`, `alamat`, `telp`, `email`) VALUES
(100, 'Starbosk', 'Russia', '097684', 'star@starbosk.com'),
(101, 'captain jack', 'karibia', '654762536745', 'cs@jack.com'),
(102, 'travelsky', 'canada', '76573469', 'cs@travelsky.com'),
(103, 'Bathoven', 'Venice', '24534654', 'marketing@bathoven.com'),
(104, 'Alizee', 'bordeaux - france', '09354721', 'marketing@alizee.com');

-- --------------------------------------------------------

--
-- Table structure for table `port`
--

CREATE TABLE IF NOT EXISTS `port` (
`id_port` int(255) NOT NULL,
  `nama_port` varchar(50) NOT NULL,
  `wilayah` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `port`
--

INSERT INTO `port` (`id_port`, `nama_port`, `wilayah`) VALUES
(1, 'Padang Bai', 'Bali'),
(2, 'Serangan', 'Bali'),
(3, 'Gili Trawangan', 'Gili Trawangan'),
(4, 'Gili Air', 'Gili Air'),
(5, 'Teluk Kodek', 'Lombok');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `departement` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `departement`) VALUES
('root', '341cf9022a3e6469098ed7a7d056071f73087d283407c3b108c6e0fd8996ef3c', 'master');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boat`
--
ALTER TABLE `boat`
 ADD PRIMARY KEY (`id_boat`), ADD KEY `ref_per` (`ref_per`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
 ADD PRIMARY KEY (`id_booking`), ADD UNIQUE KEY `kode_unik` (`kode_unik`), ADD KEY `id_jadwal` (`id_jadwal`), ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
 ADD PRIMARY KEY (`id`), ADD KEY `id_kapal` (`id_kapal`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
 ADD PRIMARY KEY (`payment_id`), ADD KEY `payment_cust_id` (`payment_cust_id`);

--
-- Indexes for table `penumpang`
--
ALTER TABLE `penumpang`
 ADD PRIMARY KEY (`id_penumpang`), ADD KEY `id_booking` (`id_booking`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
 ADD PRIMARY KEY (`id_per`);

--
-- Indexes for table `port`
--
ALTER TABLE `port`
 ADD PRIMARY KEY (`id_port`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
MODIFY `id_booking` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
MODIFY `id_customer` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
MODIFY `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `penumpang`
--
ALTER TABLE `penumpang`
MODIFY `id_penumpang` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `port`
--
ALTER TABLE `port`
MODIFY `id_port` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `boat`
--
ALTER TABLE `boat`
ADD CONSTRAINT `boat_ibfk_1` FOREIGN KEY (`ref_per`) REFERENCES `perusahaan` (`id_per`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_kapal`) REFERENCES `boat` (`id_boat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`payment_cust_id`) REFERENCES `booking` (`id_booking`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penumpang`
--
ALTER TABLE `penumpang`
ADD CONSTRAINT `penumpang_ibfk_1` FOREIGN KEY (`id_booking`) REFERENCES `booking` (`id_booking`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
