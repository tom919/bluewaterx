<?php
include"header2.php";
?>

<!-- PAGE HEADER -->
<div class="page_header">
  <div class="page_header_parallax">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3><span>Blog</span>Gili Trawangan and around</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="bcrumb-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="bcrumbs">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- INNER CONTENT -->
<div class="inner-content">
  <div class="container">
    <?php
    // set up or arguments for our custom query
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $query_args = array(
      'post_type' => 'post',
      'category_name' => '',
      'posts_per_page' => 3,
      'paged' => $paged
    );
    // create a new instance of WP_Query
    $the_query = new WP_Query( $query_args );
  ?>

  <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>




    <div class="row">
      <div class="col-md-4">
        <div class="hb-info">
          <div class="hb-thumb">
            <?php

add_image_size( 'custox', 360, 196, true );
the_post_thumbnail('custox');

$ids=get_the_ID();
?>
<div class="date-meta">
              <?php the_date('Y-m-d'); ?>
            </div>

          </div>
          <h4><a href="blog_detail?id=<?php echo $ids; ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
        <p>	<?php the_excerpt(); ?></p>


        </div>
      </div>



<?php endwhile; ?>
<!-- Add the pagination functions here. -->

<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>

<?php else: ?>
 <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

  </div>
</div>





<?php include"footer.php"?>
