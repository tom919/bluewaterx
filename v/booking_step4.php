<?php
include"header2.php";
?>

<!-- PAGE HEADER -->
<div class="page_header">
  <div class="page_header_parallax">
    <div class="container">
      <div class="row">
        <div class="col-md-12 head-bg">
          <h3>Step 4 - Payment </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="bcrumb-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="bcrumbs">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- INNER CONTENT -->
<?php


?>
<div class="inner-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <div class="blog-single">
          <article class="blogpost">
            <h2 class="post-title"><?php echo $title; ?></h2>

            <div class="space"></div>

            <!--start content -->

            <?php
            $booking_ident=$_POST['booking_ident'];
            $booking_code=$_POST['booking_code'];
            $booking_code_r=$_POST['booking_code_r'];
          $cust_name=$_POST['cust_name'];
          $cust_address=$_POST['cust_address'];
          $cust_city=$_POST['cust_city'];
          $cust_country=$_POST['cust_country'];
          $cust_phone=$_POST['cust_phone'];
          $cust_email=$_POST['cust_email'];
          $cust_pickup=$_POST['cust_pickup'];
          $cust_pickup_address=$_POST['cust_pickup_address'];
          $cust_pickup_r=$_POST['cust_pickup_r'];
          $cust_pickup_address_r=$_POST['cust_pickup_address_r'];
          $cust_payment=$_POST['cust_payment'];
          $cust_payment_id=$_POST['cust_payment_id'];

          $trip=$_POST['trip'];
          $r_trip=$_POST['r_trip'];
          $departure_date=$_POST['departure_date'];
          $arrival_date=$_POST['arrival_date'];
          $adult_amount=$_POST['adult_amount'];
          $child_amount=$_POST['child_amount'];

          $BX_NAME=$_POST['BX_NAME'];        // array
          $BX_age=$_POST['BX_age'];	   // array
          $BX_gender=$_POST['BX_gender'];    // array


          /*--- input data customer---*/
          $cust_val="'$cust_name','$cust_address','$cust_city','$cust_country','$cust_phone','$cust_email'";
          $xst=input_customer($cust_val);

          $xst_a=select_customer($cust_name,$cust_email);
          $row_xst=mysql_fetch_array($xst_a);

          /*--- input data booking --*/
          $tanggal_booking=date("Y-m-d");
          $booking_status="unconfirmed";
          $cust_id=$row_xst['id_customer'];




          /*--(kode_unik,id_customer,id_jadwal,tanggal,tanggal_berangkat,tanggal_kembali,jenis_pembayaran,akun_pembayaran,opsi_pickup,alamat_pickup,status)--*/
          $booking_val="'$booking_ident','$booking_code','$cust_id','$trip','$tanggal_booking','$departure_date','$cust_pickup','$cust_pickup_address','$booking_status'";
          $xst2=input_booking($booking_val);
          if($r_trip!=="")
          {
            $booking_val_r="'$booking_ident','$booking_code_r','$cust_id','$r_trip','$tanggal_booking','$arrival_date','$cust_pickup_r','$cust_pickup_address_r','$booking_status'";
            $xst2_r=input_booking($booking_val_r);
          }




          /*---- input data pembayaran----*/
          /*$cust_payment_amount="";
          $payment_status="unpaid";
          $bayar_val="'$cust_payment_type','$cust_payment_acc','$cust_payment_amount','$cust_booking_id','$payment_status'";
          $xst3=input_pembayaran($bayar_val);
          */

          /*---- kirim email ---*/




          if($xst2==10){

          echo "<div class='alert alert-success'>

              <h4><i class='icon fa fa-check'></i> Booking Success</h4>
              <br>Your Booking payment detail has been send to your email<br>
              <br>We will send your E-TICKET after receive Your payment<br>
              <div class='clearfix'></div>
            </div>
          </div>";
          }else{

           echo "error level 1";

          }


          ?>

          <table class="table">
            <thead>
              <tr class="success">
                <td><b>Customer Details</b></td><td><b>Booking Date : <?php echo $tanggal_booking;?></b></td><td></td>
              </tr>
            </thead>
  <tbody>
    <tr>
      <td>Customer Name</td>
      <td><?php echo $cust_name;?></td>

    </tr>
    <tr >
      <td>Address</td>
      <td><?php echo $cust_address;?></td>

    </tr>
    <tr>
      <td>City</td>
      <td><?php echo $cust_city;?></td>

    </tr>
    <tr>
      <td>Country</td>
      <td><?php echo $cust_country;?></td>

    </tr>
    <tr>
      <td>Phone</td>
      <td><?php echo $cust_phone;?></td>

    </tr>
    <tr>
      <td>Email</td>
      <td><?php echo $cust_email;?></td>

    </tr>
       <td>Payment Type</td>
       <td><?php echo$cust_payment;?></td>
     </tr>
     <tr>
       <td>Payment ID</td>
       <td><?php echo$cust_payment_id;?></td>
     </tr>
  </tbody>
</table>
<br>
<table class="table">
  <caption>Passenger List</caption>
  <thead>
    <tr>

    <th>Name</th>
    <th>Age</th>
    <th>Gender</th>
  </tr>
  </thead>
  <tbody>
<?php foreach($BX_NAME as $a => $b){ ?>
	<tr>


		<td>

			<?php echo $BX_NAME[$a]; ?></label>
		</td>
		<td>

			<?php echo $BX_age[$a]; ?>
		</td>
		<td>

    <?php echo $BX_gender[$a]; ?>
		</td>

	</tr>
<?php } ?>
</tbody>
</table>
<!-- booking detail -->
<?php echo "<h3> Booking ID ".$booking_ident."</h3>" ?>
<br><br>
<h3>DEPART</h3>
<?php
$x=tampil_harga($trip);
$xr=mysql_fetch_array($x);
?>
<table class="table table-hover">
<tbody>
<tr>
<td>Date</td><td>:&nbsp;<?php echo $departure_date;?></td><td>Estimated Time Departure</td><td>:&nbsp;<?php echo $xr['keberangkatan'];?></td></tr>
<tr><td><!--Booking Code--></td><td><!--:&nbsp;<b><?php echo $booking_code; ?>--></b></td><td>Estimated Time Arrival</td><td>:&nbsp;<?php echo $xr['kedatangan'] ?></td>
</tr>
<tr>
<td>Pickup Address</td><td>:&nbsp;<?php echo $cust_pickup_address;?></td><td>Boat Name</td>
<td>:&nbsp;
<?php   $z=tampil_ref_boat_jadwal($trip);
$zr=mysql_fetch_array($z);
echo $zr['nama_boat'];
?>
</td>
</tr>
<tr>
  <td>Fare Details</td>
  <td>
    Passenger amount<br>
    <?php echo $adult_amount."&nbsp;Adult"; ?>
    <br>
    <?php echo $child_amount."&nbsp;Child"; ?>
  </td>
<td>
  Ticket Price<br>
  <?php
echo "Rp. ".$xr['tarif_dewasa'];
echo "<br>";
echo "Rp. ".$xr['tarif_anak'];
?></td>
<td>
  <text class="pull-right">
  Sub totals<br>
<?php
    $subtotal_trip=$adult_amount*$xr['tarif_dewasa'];
    echo "Rp. ".$subtotal_trip;
?></text>
<br><br>
<text class="pull-right"><?php
    $c_subtotal_trip=$child_amount*$xr['tarif_anak'];
    echo "Rp. ".$c_subtotal_trip;
?></text>
</td>
</tr>
</tbody>
</table>

<?php if($r_trip!=""){
  ?>

<h3>RETURN</h3>



<?php
$y=tampil_harga($r_trip);
$yr=mysql_fetch_array($y);
?>
<table class="table table-hover">
<tbody>
<tr>
<td>Date</td><td>:&nbsp;<?php echo $arrival_date;?></td><td>Estimated Time Departure</td><td>:&nbsp;<?php echo $yr['keberangkatan'];?></td></tr>
<tr><td>Booking Code</td><td>:&nbsp;<b><?php echo $kode_r; ?></b></td><td>Estimated Time Arrival</td><td>:&nbsp;<?php echo $yr['kedatangan'] ?></td>
</tr>
<tr>
<td>Pickup Address</td><td>:&nbsp;<?php echo $cust_pickup_address_r;?></td><td>Boat Name</td>
<td>:&nbsp;
<?php   $w=tampil_ref_boat_jadwal($r_trip);
$zr=mysql_fetch_array($w);
echo $zr['nama_boat'];
?>
</td>
</tr>
<tr>
  <td>Fare Details</td>
  <td>
    Passenger amount<br>
    <?php echo $adult_amount."&nbsp;Adult"; ?>
    <br>
    <?php echo $child_amount."&nbsp;Child"; ?>
  </td>
<td>
  Ticket Price<br>
  <?php
echo "Rp. ".$yr['tarif_dewasa'];
echo "<br>";
echo "Rp. ".$yr['tarif_anak'];
?></td>
<td>
  <text class="pull-right">
  Sub totals<br>
  <?php
  $subtotal_r_trip=$adult_amount*$yr['tarif_dewasa'];
  echo "Rp. ".$subtotal_r_trip;
  ?></text>
<br><br>
<text class="pull-right"><?php
    $c_subtotal_r_trip=$child_amount*$yr['tarif_anak'];
    echo "Rp. ".$c_subtotal_r_trip;
?></text>
</td>
</tr>
</tbody>
</table>
<?php } ?>

<br>
<table class="table">
  <tbody><tr class="primary">
<td>Grand Total</td>
<td>
<text class="pull-right"> <b> <?php
$grandtotal=$subtotal_trip+$subtotal_r_trip+$c_subtotal_trip+$c_subtotal_r_trip;
echo"Rp. ".$grandtotal;
?></b></text>
</td>
</tr>
</tbody>
</table>


<!-- payment -->

<h3><u>Payment</u></h3>

<?php
/*--payment_type,payment_acc,payment_amount,payment_cust_id,payment_status--*/
$trip_total=$subtotal_trip+$c_subtotal_trip;
$payment_status="unpaid";
$sb1=select_booking($booking_code,$tanggal_booking);
$row_sb1=mysql_fetch_array($sb1);
$payment_booking_id=$row_sb1['id_booking'];
$adult_ticket_price=$xr['tarif_dewasa'];
$child_ticket_price=$xr['tarif_anak'];
$payment_val="'$cust_payment','$cust_payment_id','$adult_amount','$child_amount','$adult_ticket_price','$child_ticket_price','$trip_total','$payment_booking_id','$payment_status'";
$p_xst2=input_pembayaran($payment_val);

if($r_trip!=="")
{
  $r_trip_total=$subtotal_r_trip+$c_subtotal_r_trip;

  $sb2=select_booking($booking_code_r,$tanggal_booking);
  $row_sb2=mysql_fetch_array($sb2);
  $r_booking_id=$row_sb2['id_booking'];
  $adult_ticket_price_r=$yr['tarif_dewasa'];
  $child_ticket_price_r=$yr['tarif_anak'];
  $r_payment_val="'$cust_payment','$cust_payment_id','$adult_amount','$child_amount','$adult_ticket_price_r','$child_ticket_price_r','$r_trip_total','$r_booking_id','$payment_status'";
  $p_xst2_r=input_pembayaran($r_payment_val);
}
  $strings=pembayaran($cust_payment,$booking_code,$grandtotal);
  echo $strings;

/*--- input penumpang --*/
foreach($BX_NAME as $a => $b){
  $p_name=$BX_NAME[$a];
  $p_age=$BX_age[$a];
  $p_gen=$BX_gender[$a];
$p_val="'$p_name','$p_age','$p_gen','$payment_booking_id'";
$p_xst2p=input_penumpang($p_val);


 }



?>

<?php


$to=$cust_email;
$subject="Your Booking ".$booking_ident;



  $m_middle="";
if($r_trip!=="")
{
  $m_middle='
<h3>RETURN</h3>
  <table class="table table-hover" border="1">
  <tbody>
  <tr>
  <td>Date</td><td>:&nbsp;'.$arrival_date.'</td><td>Estimated Time Departure</td><td>:&nbsp;'.$yr['keberangkatan'].'</td></tr>
  <tr><td>Booking Code</td><td>:&nbsp;<b>'.$kode_r.'</b></td><td>Estimated Time Arrival</td><td>:&nbsp;'.$yr['kedatangan'].'</td>
  </tr>
  <tr>
  <td>Pickup Address</td><td>:&nbsp;'.$cust_pickup_address_r.'</td><td>Boat Name</td>
  <td>:&nbsp;
  '.$zr['nama_boat'].'
  </td>
  </tr>
  <tr>
    <td>Fare Details</td>
    <td>
      Passenger amount<br>
      '.$adult_amount.'&nbsp;Adult
      <br>
      '.$child_amount.'&nbsp;Child
    </td>
  <td>
    Ticket Price<br>

  Rp. '.$yr['tarif_dewasa'].'<br>Rp. '.$yr['tarif_anak'].'</td>
  <td>
    <text class="pull-right">
    Sub totals<br>
    Rp. '.$subtotal_r_trip.'</text>
  <br>
  <text class="pull-right">
      Rp. '.$c_subtotal_r_trip.'</text>
  </td>
  </tr>
  </tbody>
  </table>
';
}



$message_all='
<html>
<body>
<img src="http://localhost/bluewaterx/r/images/basic/logo3.png" width="200px">
<h3>Booking Payment</h3>
<table class="table">
  <thead>
    <tr class="success">
      <td><b>Customer Details</b></td><td><b>Booking Date : '.$tanggal_booking.'</b></td><td></td>
    </tr>
  </thead>
<tbody>
<tr>
<td>Customer Name</td>
<td>'.$cust_name.'</td>

</tr>
<tr >
<td>Address</td>
<td>'.$cust_address.'</td>

</tr>
<tr>
<td>City</td>
<td>'.$cust_city.'</td>

</tr>
<tr>
<td>Country</td>
<td>'.$cust_country.'</td>

</tr>
<tr>
<td>Phone</td>
<td>'.$cust_phone.'</td>

</tr>
<tr>
<td>Email</td>
<td>'.$cust_email.'</td>

</tr>
<td>Payment Type</td>
<td>'.$cust_payment.'</td>
</tr>
<tr>
<td>Payment ID</td>
<td>'.$cust_payment_id.'</td>
</tr>
</tbody>
</table>


<br>
<h3> Booking ID '.$booking_ident.'</h3>
<br><br>
<h3>DEPART</h3>
<table class="table table-hover" border="1">
<tbody>
<tr>
<td>Date</td><td>:&nbsp;'.$departure_date.'</td><td>Estimated Time Departure</td><td>:&nbsp;'.$xr['keberangkatan'].'</td></tr>
<tr><td></td><td></td><td>Estimated Time Arrival</td><td>:&nbsp;'.$xr['kedatangan'].'</td>
</tr>
<tr>
<td>Pickup Address</td><td>:&nbsp;'.$cust_pickup_address.'</td><td>Boat Name</td>
<td>:&nbsp;
'.$zr['nama_boat'].'
</td>
</tr>
<tr>
  <td>Fare Details</td>
  <td>
    Passenger amount<br>
    '.$adult_amount.'&nbsp;Adult
    <br>
    '.$child_amount.'&nbsp;Child
  </td>
<td>
  Ticket Price<br>
  Rp. '.$xr['tarif_dewasa'].'
<br>
Rp. '.$xr['tarif_anak'].'</td>
<td>
  <text class="pull-right">
  Sub totals<br>
    Rp. '.$subtotal_trip.'</text>
<br>
<text class="pull-right">
    Rp. '.$c_subtotal_trip.'
</text>
</td>
</tr>
</tbody>
</table>


<br>
'.$m_middle.'
<br>
<table class="table">
  <tbody><tr class="primary">
<td>Grand Total</td>
<td>
<text class="pull-right"> <b> Rp. '.$grandtotal.'</b></text>
</td>
</tr>
</tbody>
</table>
<br>

<br>
'.$strings.'
</body>
</html>
';


  kirim_email_booking($to,$subject,$message_all);
?>







          <!--- end of content -->
          </article>
        </div>
        <div class="padding70">

        </div>


        <div class="space60"></div>
        <div class="clearfix prevnext">

        </div>
      </div>
    </div>
  </div>
</div>





<?php include"footer.php"; ?>
