<?php
include"header2.php";
?>

<!-- PAGE HEADER -->
<div class="page_header">
  <div class="page_header_parallax">
    <div class="container">
      <div class="row">
        <div class="col-md-12 head-bg">
          <h3>Step 1 - Check Avability</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="bcrumb-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="bcrumbs">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- INNER CONTENT -->
<?php


?>
<div class="inner-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="blog-single">
          <article class="blogpost">
            <h2 class="post-title"><?php echo $title; ?></h2>

            <div class="space2"></div>

            <!--start content -->



            <form method="post" action="booking_step2">

            <?php
            $departure_place=$_POST['departure_place'];
            $arrival_place=$_POST['arrival_place'];
            $departure_date=$_POST['departure_date'];
            $arrival_date=$_POST['arrival_date'];
            $adult_amount=$_POST['adult'];
            $child_amount=$_POST['child'];
            $required_seat=$adult_amount+$child_amount;
            echo "<input type='hidden' name='departure_date' value='$departure_date'>";
            echo "<input type='hidden' name='arrival_date' value='$arrival_date'>";
            echo "<input type='hidden' name='adult_amount' value='$adult_amount'>";
            echo "<input type='hidden' name='child_amount' value='$child_amount'>";
            echo"<p>From $departure_place to $arrival_place &nbsp;";
            echo"At $departure_date";
            if($arrival_date!==""){
            echo ", Return at $arrival_date";
            }
            echo"<br>";
            echo "For $adult_amount Adult &nbsp;";
            if($child_amount!==""){
            echo "and $child_amount Child";
           }
             echo"</p><br>";
            echo"<div class='col-md-6'>
            <h4>Departure </h4>";

            $query=search_avability($departure_place,$arrival_place);
            if(mysql_num_rows($query) < 1){
              echo "No Boat Trip Available";
            }else{

              echo " <table class='table table-striped'>
     <thead class='thead-default'>
       <tr>
         <th>Boat</th>
         <th>Departure</th>
         <th>Arrival</th>
         <th>Price</th>
       </tr>
     </thead>
     <tbody>";
                  while($row=mysql_fetch_array($query)){
                    $z=tampil_ref_boat($row['id_kapal']);
                    $zr=mysql_fetch_array($z);

                        echo" <tr>
                           <td>$zr[nama_boat]</td>
                           <td>$row[port_keberangkatan]<br> $row[keberangkatan]</td>
                           <td>$row[port_kedatangan]<br> $row[kedatangan]</td>
                           <td>Adult: $row[tarif_dewasa]<br> Children: $row[tarif_anak]</td>";
                           $cp=check_capacity($row['id_kapal']);
                           $cpr=mysql_fetch_array($cp);
                           $cpx=$cpr['kapasitas'];
                           $ct=check_onboard_amount($row['id'],$departure_date);
                           $ctr=mysql_num_rows($ct);
                           $ctrx=$ctr;
                           $remain_seat=$cpx-$ctrx;

                           if($remain_seat<$required_seat)
                           {
                             echo"<td>Full</td>";
                             $ctrl=1;
                           }
                           else
                           {
                           echo "<td><input type='radio' name='trip' value='$row[id]' required></td>";
                            }
                         echo"</tr>";
                    }

                    echo"   </tbody>
                     </table>";
            }
            echo "</div><div class='col-md-6'><h4>Return</h4>";

            if($arrival_date==""){
              echo"";
            }else{
              $r_departure=$arrival_place;
              $r_arrival=$departure_place;
              $query=search_avability($r_departure,$r_arrival);
              if(mysql_num_rows($query) < 1){
                echo "No Boat Trip Available";
              }else{

                echo "<table class='table table-striped'>
       <thead class='thead-default'>
         <tr>
           <th>Boat</th>
           <th>Departure</th>
           <th>Arrival</th>
           <th>Price</th>
         </tr>
       </thead>
       <tbody>";
                    while($rowt=mysql_fetch_array($query)){
                      $z=tampil_ref_boat($rowt['id_kapal']);
                      $zr=mysql_fetch_array($z);

                          echo" <tr>
                             <td>$zr[nama_boat]</td>
                             <td>$rowt[port_keberangkatan]<br> $rowt[keberangkatan]</td>
                             <td>$rowt[port_kedatangan]<br> $rowt[kedatangan]</td>
                             <td>Adult: $rowt[tarif_dewasa]<br> Children: $rowt[tarif_anak]</td>";

                             $cp_t=check_capacity($rowt['id_kapal']);
                             $cpr_t=mysql_fetch_array($cp_t);
                             $cpx_t=$cpr_t['kapasitas'];
                             $ct_t=check_onboard_amount($rowt['id'],$arrival_date);
                             $ctr_t=mysql_num_rows($ct_t);
                             $ctrx_t=$ctr_t;
                             $remain_seat_t=$cpx_t-$ctrx_t;

                             if($remain_seat_t<$required_seat)
                             {
                               echo"<td>Full</td>";
                               $ctrl_r=1;
                             }
                             else
                             {
                             echo "<td><input type='radio' name='r_trip' value='$rowt[id]' required></td>";
                              }


                           echo"</tr>";
                      }

                      echo"   </tbody>
                       </table>";
              }
            }
            echo"</div>";
            ?>

            <div class="form-group">
              <?php if($ctrl OR $ctrl_r !="" ){
                echo "Please change Your search";
              }else{
                ?>
              <button type="submit" class="btn btn-primary pull-right">Continue &nbsp;<i class="fa fa-hand-o-right" aria-hidden="true"></i>
</button>
<?php } ?>
            </div>
          </form>
          <!--- end of content -->
          </article>
        </div>
        <div class="padding70">

        </div>


        <div class="space60"></div>
        <div class="clearfix prevnext">

        </div>
      </div>
    </div>
  </div>
</div>





<?php include"footer.php"?>
