<?php
include"../b/wp-load.php";
include_once"../r/controller.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
	<title>Gili X-press</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">


	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="../r/css/bootstrap.min.css" type="text/css">

	<!-- Styles -->
	<link rel="stylesheet" href="../r/js/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../r/js/owl-carousel/owl.theme.css">
	<link rel="stylesheet" href="../r/js/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="../r/js/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="../r/js/flexslider/flexslider.css">
	<link rel="stylesheet" href="../r/js/isotope/isotope.css">
	<link rel="stylesheet" href="../r/css/jquery-ui.css">
	<link rel="stylesheet" href="../r/js/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../r/css/style.css">
		<link rel="stylesheet" href="../r/css/bootstrap-datepicker.min.css">

	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Dosis:400,200,300,500,600,700,800" rel="stylesheet" type="text/css">

	<!-- Icon Fonts -->
	<link href="../r/css/icomoon/style.css" rel="stylesheet"  type="text/css">
	<link rel="stylesheet" href="../r/font-awesome/css/font-awesome.min.css" type="text/css">

	<!-- SKIN -->
	<link rel="stylesheet" href="../r/css/color-scheme/default-blue.css" type="text/css">

	<script>
	function addRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		if(rowCount < 10){                            // limit the user from creating fields more than your limits
			var row = table.insertRow(rowCount);
			var colCount = table.rows[0].cells.length;
			for(var i=0; i <colCount; i++) {
				var newcell = row.insertCell(i);
				newcell.innerHTML = table.rows[0].cells[i].innerHTML;
			}
		}else{
			 alert("Maximum Passenger per ticket is 10");

		}
	}

	function deleteRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		for(var i=0; i<rowCount; i++) {
			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			if(null != chkbox && true == chkbox.checked) {
				if(rowCount <= 1) {               // limit the user from removing all the fields
					alert("Cannot Remove all the Passenger.");
					break;
				}
				table.deleteRow(i);
				rowCount--;
				i--;
			}
		}
	}
	</script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
           <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.2/jquery.xdomainrequest.min.js"></script>
	<![endif]-->

</head>
<body id="header6">

<div id="page-top"></div>
<div class="outer-wrapper">
	<div class="header-wrap">

		<!-- HEADER -->
		<header id="header-main">
		<div class="container">
			<div class="navbar yamm navbar-default">
				<div class="navbar-header">
					<button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a href="index.html" class="navbar-brand"><img src="../r/images/basic/logo.png" width="40" alt=""/></a>
				</div>

				<!-- SEARCH -->
				<div class="header-x pull-right">
					<div class="s-search">
						<div class="ss-trigger"><i class="icon-search2"></i></div>
						<div class="ss-content">
							<span class="ss-close icon-cross2"></span>
							<div class="ssc-inner">
								<form>
									<input type="text" placeholder="Type Search text here...">
									<button type="submit"><i class="icon-search2"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
					<ul class="nav navbar-nav">
						<li class="page-scroll"><a href="home">Home</a></li>
						<li class="page-scroll"><a href="home#1">About Us</a></li>
						<li class="page-scroll"><a href="home#2">Booking</a></li>
						<li class="page-scroll"><a href="home#3">Destination</a></li>
						<li class="page-scroll"><a href="blog">Blog</a></li>
						<li class="page-scroll"><a href="home#7">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>


	</div>
