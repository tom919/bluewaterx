<!-- FOOTER -->
<footer>
  <div class="container">
    <div class="row">
    <!--  <div class="col-md-4">
        <h4 class="space30">About us</h4>
        <img src="../r/images/basic/logo.png" class="img-responsive space20" width="262" alt=""/>
        <p>Lorem ipsum dolor sit amet consec tetur elit vel quam ligula. Duis vel pulvinar diam in lacus non nisl commodo convallis.</p>
        <p>Phasellus rutrum urna ut nibh congue, ut vehicula nibh ultricies.</p>
      </div>
      <div class="col-md-4">

      </div>
      <div class="col-md-4">
        <h4 class="space30">Contact</h4>
      <ul class="c-info">
          <li><i class="fa fa-map-marker"></i> 72 Wall street Rd<br>Some county<br>Newyork 20001
          </li>
          <li>
          </li>
          <li><i class="fa fa-phone"></i> (012) 345 5678 910</li>
          <li><i class="fa fa-envelope-o"></i> support@dk-themes.com</li>
          <li><i class="fa fa-skype"></i> myskypeid</li>
        </ul>
        <div class="clearfix space10"></div>
      </div>-->

    </div>
  </div>
</footer>

<!-- FOOTER COPYRIGHT -->
<div class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <p>&copy; Copyright 2017. Gili X-press. Designed by <a href="#" target="_blank"> Thomas B Prabowo</a></p>
      </div>
      <div class="col-md-4">
        <div class="f-social pull-right">
          <a class="fa fa-twitter" href="<?php echo $global_twitter;?>" target="_blank"></a>
          <a class="fa fa-facebook" href="<?php echo $global_facebook; ?>" target="_blank"></a>
          <a class="fa fa-linkedin" href="<?php echo $global_linkedin;?>" target="_blank"></a>
          <a class="fa fa-google-plus" href="<?php echo $global_googleplus; ?>" target="_blank"></a>
          <a class="fa fa-skype" href="<?php echo $global_skype; ?>"></a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<!-- STYLE SWITCHER
============================================= -->
<!--<div class="b-settings-panel">
<div class="settings-section">
  <span>
  Boxed
  </span>
  <div class="b-switch">
    <div class="switch-handle"></div>
  </div>
  <span>
  Wide
  </span>
</div>
<hr class="dashed" style="margin: 15px 0px;">
<h5>Main Background</h5>
<div class="settings-section bg-list">
  <div class="bg-pattern1"></div>
  <div class="bg-pattern2"></div>
  <div class="bg-pattern6"></div>
  <div class="bg-pattern10"></div>
  <div class="bg-pattern16"></div>
  <div class="bg-pattern4"></div>
  <div class="bg-pattern5"></div>
  <div class="bg-pattern7"></div>
  <div class="bg-pattern9"></div>
  <div class="bg-pattern11"></div>
  <div class="bg-pattern12"></div>
  <div class="bg-pattern13"></div>
  <div class="bg-pattern17"></div>
  <div class="bg-pattern8"></div>
  <div class="bg-pattern14"></div>
  <div class="bg-pattern15"></div>
  <div class="bg-pattern3"></div>
  <div class="bg-pattern18"></div>
</div>
<hr class="dashed" style="margin: 15px 0px;">
<h5>Color Scheme</h5>
<div class="settings-section color-list">
  <div data-src="../r/css/color-scheme/moderate-green.css" style="background: #8ec249"></div>
  <div data-src="../r/css/color-scheme/vivid-blue.css" style="background: #228dff"></div>
  <div data-src="../r/css/color-scheme/orange.css" style="background: #fa6900"></div>
  <div data-src="../r/css/color-scheme/brown.css" style="background: #a68c69"></div>
  <div data-src="../r/css/color-scheme/yellow.css" style="background: #fabe28"></div>
  <div data-src="../r/css/color-scheme/violet.css" style="background: #ba01ff"></div>
  <div data-src="../r/css/color-scheme/strong-cyan.css" style="background: #00b9bd"></div>
  <div data-src="../r/css/color-scheme/soft-cyan.css" style="background: #4bd5ea"></div>
  <div data-src="../r/css/color-scheme/red.css" style="background: #ff0104"></div>
  <div data-src="../r/css/color-scheme/lite-brown.css" style="background: #f3a76d"></div>
  <div data-src="../r/css/color-scheme/lime-green.css" style="background: #3bdbad"></div>
  <div data-src="../r/css/color-scheme/light-voilet.css" style="background: #aaa5ff"></div>
  <div data-src="../r/css/color-scheme/gray-green.css" style="background: #697060"></div>
  <div data-src="../r/css/color-scheme/gray-cyan.css" style="background: #aeced2"></div>
  <div data-src="../r/css/color-scheme/de-green.css" style="background: #b6cd71"></div>
  <div data-src="../r/css/color-scheme/cream.css" style="background: #e0d6b2"></div>

</div>
<div class="btn-settings"></div>
</div>-->
<!-- END STYLE SWITCHER
============================================= -->

<!-- jQuery -->
<script src="../r/js/jquery.js"></script>

<!-- Plugins -->
<script src="../r/js/bootstrap.min.js"></script>
<script src="../r/js/menu.js"></script>
<script src="../r/js/owl-carousel/owl.carousel.min.js"></script>
<script src="../r/js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="../r/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="../r/js/jquery.easing.min.js"></script>
<script src="../r/js/isotope/isotope.pkgd.js"></script>
<script src="../r/js/jflickrfeed.min.js"></script>
<script src="../r/js/tweecool.js"></script>
<script src="../r/js/flexslider/jquery.flexslider.js"></script>
<script src="../r/js/easypie/jquery.easypiechart.min.js"></script>
<script src="../r/js/jquery-ui.js"></script>
<script src="../r/js/jquery.appear.js"></script>
<script src="../r/js/jquery.inview.js"></script>
<script src="../r/js/jquery.countdown.min.js"></script>
<script src="../r/js/jquery.sticky.js"></script>
<script src="../r/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="../r/js/jquery.easing/jquery.easing.js"></script>

<script src="../r/js/main.js"></script>
<script src="../r/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN1h83IjszAf3af-ubtBy1nBOSE-utBdw&sensor=false"></script>
<script src="../r/js/gmaps/greyscale.js"></script>
<script>
$("#rControl").click(function() {
  $("#return").attr("disabled", !this.checked);
});
</script>
<script>
$("#rControl2").click(function() {
  $("#pickup").attr("disabled", !this.checked);
});
</script>
<script>
$("#rControl3").click(function() {
  $("#pickup_r").attr("disabled", !this.checked);
});
</script>
</body>
</html>
