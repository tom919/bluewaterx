<?php
	include 'header.php';
	require_once'../r/setting.php';

?>




	<!-- SLIDER -->
	<div class="slider-wrap">

		<div class="tp-banner-container">




			<div class="tp-banner" >

				<ul>
					<?php

					$batas = 5;
				$pg = isset( $_GET['pg'] ) ? $_GET['pg'] : "";

				if ( empty( $pg ) ) {
				$posisi = 0;
				$pg = 1;
				} else {
				$posisi = ( $pg - 1 ) * $batas;
				}

					$query=tampil_slider($posisi,$batas);

					?>
					  <?php  while($row=mysql_fetch_array($query)){ ?>

					<!-- SLIDE  -->
										<li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-saveperformance="on"  data-title="Ken Burns Slide">
						<!-- MAIN IMAGE -->
						<img src="../r/images/dummy.png"  alt="2" data-lazyload="../r/images/slider/<?PHP echo $row['foto'] ?>.jpg" data-bgposition="right top" data-kenburns="off" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center bottom">
						<div class="tp-caption tentered_white_huge lft tp-resizeme" data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600" data-y="280" data-hoffset="0" data-x="right"
							style="    color: #fff;
							text-transform: uppercase;
							font-size: 40px;
							letter-spacing: 6px;
							font-family: Montserrat;
							font-weight: 400;
							"
							>
							<?php echo $row['caption'];?>
						</div>
						<div class="tp-caption tentered_white_huge lfb tp-resizeme" data-endspeed="300" data-easing="Power4.easeOut" data-start="800" data-speed="600" data-y="350" data-hoffset="0" data-x="right"
							style="    color: #fff;
							font-size: 13px;
							text-transform: uppercase;
							letter-spacing: 10px;
							"
							>
							<?php echo $row['content'];?>
						</div>
						<a href="<?php echo $row['link'];?>" target="_blank" class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
							data-x="right"
							data-y="400"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="500"
							data-start="1200"
							data-easing="Power3.easeInOut"
							data-splitin="none"
							data-splitout="none"
							data-elementdelay="0.1"
							data-endelementdelay="0.1"
							data-linktoslide="next"
							style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;padding:15px 28px;
							color: #fff;
							text-transform: uppercase;
							border: none;
							background:#000;
							font-size: 12px;
							letter-spacing: 3px;
							font-family: Montserrat;
							border-radius: 0px;
							display: table;
							transition: .4s;
							;"><?php echo $row['link_title'];?></a>
					</li>

					<?php } ?>


					</ul>

				<div class="tp-bannertimer"></div>
			</div>
		</div>

	</div>

	<!-- INNER CONTENT -->

	<div class="container-fluid no-padding" >
<!--- startofstep-->
		<div class="padding80 border-top">
			<div class="container" id="2">
			<div class="col-md-8 col-md-offset-2 text-center space50">
				<h2>Booking Step</h2>
				<p>here is booking step</p>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="fa fa-calendar"></i>
						</div>
						<div class="feature-box-info">
							<h4>Step 1 - Check Avability</h4>
							<p>Check available trip route</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="fa fa-user"></i>
						</div>
						<div class="feature-box-info">
							<h4>Step 2 - Complete Form</h4>
							<p>Fill form with necessary information</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="fa fa-check-square"></i>
						</div>
						<div class="feature-box-info">
							<h4>Step 3 - confirm</h4>
							<p>Check again your booking details</p>
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="feature-box">


						<div class="feature-box-icon">
							<i class="fa fa-usd"></i>
						</div>
						<div class="feature-box-info">
							<h4>Step 4 - pay</h4>
							<p>Pay your booking invoice</p>
						</div>


					</div>
				</div>




			</div>

			<div class="space20"></div>

		</div></div>
		<hr>
<!--- endofstep-->

<!--- endoffeaturedarticle-->
<div class="container padding70" >
	<div class="text-center space40" id="4">
		<h2 class="title uppercase">The Gili Trawangan</h2>
		<p> Gili Trawangan, Gili Meno and Gili Air. <br>This archipelago of three small coral isles are rapidly becoming one of the most popular destinations for visitors to Bali and Lombok.</p>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<div class="space90"></div>
			<ul class="features-left">
				<li>
					<i class="icon-compass"></i>
					<h3>Diving</h3>
					<p> It’s probably the classic at the Gilis. No wonder, because the islands have a very interesting underwater world. There are diving schools on each of the 3 islands, although most of them are on Gili Trawangan. You can borrow snorkel equipment almost on every corner.</p>
				</li>
				<li>
					<i class="fa fa-rocket"></i>
					<h3>Surfing</h3>
					<p>There are a few surf spots on the Gilis, mainly in the southeast of Gili Trawangan, but also on Gili Meno and Gili Air. </p>
				</li>
				<li>
					<i class="icon-strategy"></i>
					<h3>Horse Riding </h3>
					<p>Ride along the sandy beaches and the quiet roads with no cars or traffic. A sunset ride along the water’s edge is a perfect way to experience the tranquility of the Gili Islands.</p>
				</li>
			</ul>
		</div>
		<div class="col-sm-4 col-sm-push-4">
			<div class="space90"></div>
			<ul class="features-right">
				<li>
					<i class="fa fa-glass"></i>
					<h3>Night Life</h3>
					<p> Gili Trawangan has been touted as the new Ibiza. Party till dawn and sleep it off at the beach. No doubt about it, if you want to party hard and drink all night Trawangan is the place to do it</p>
				</li>
				<li>
					<i class="fa fa-video-camera"></i>
					<h3>Outdoor Cinema</h3>
					<p> At the main beach of Gili Trawangan in the evening there is the possibility to watch movies at a kind of mini open-air cinema</p>
				</li>
				<li>
					<i class="icon-camera"></i>
					<h3>Beautiful Beach</h3>
					<p> The beaches of the Gilis are still powdery white, the water a beautiful clear blue and they are positioned perfectly for sunsets over Bali’s Mt Agung and sunrise over Lombok’s Mt Rinjani. </p>
				</li>
			</ul>
		</div>
		<div class="col-sm-4 col-sm-pull-4">
			<div> <img src="../r/images/other/3.png" class="img-responsive center-block" alt=""> </div>
		</div>
	</div>
</div>
<!-- endoffeaturedarticle-->


	</div>


		<div class="clearfix"></div>

		<!--
		<div class="padding80 border-top">
			<div class="container" id="2">
			<div class="col-md-8 col-md-offset-2 text-center space50">
				<h2>Our Services</h2>
				<p>Sed dapibus, leo ut placerat bibendum, ligula ligula consectetur eros, sed efficitur justo ex ut risus. Integer nec eros non elit finibus dictum quis sit amet augue.</p>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="icon-monitor"></i>
						</div>
						<div class="feature-box-info">
							<h4>Web Design</h4>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing metus elit.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="icon-layout"></i>
						</div>
						<div class="feature-box-info">
							<h4>Branding</h4>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing metus elit.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="icon-cog3"></i>
						</div>
						<div class="feature-box-info">
							<h4>SEO</h4>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing metus elit.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="space20"></div>
			<div class="row">
				<div class="col-md-4">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="icon-camera"></i>
						</div>
						<div class="feature-box-info">
							<h4>Photography</h4>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing metus elit.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="icon-lightbulb"></i>
						</div>
						<div class="feature-box-info">
							<h4>Internet Marketing</h4>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing metus elit.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-box">
						<div class="feature-box-icon">
							<i class="icon-strategy"></i>
						</div>
						<div class="feature-box-info">
							<h4>UI Design</h4>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing metus elit.</p>
						</div>
					</div>
				</div>
			</div>
		</div></div>-->
	<!--	<div class="pattern-grey">
		<div id="stats1" class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="stats1-info">
						<i class=" icon-camera"></i>
						<p><span class="count count1">499</span></p>
						<h2>Photos Taken</h2>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="stats1-info">
						<i class="icon-lock"></i>
						<p><span class="count count1">1123</span></p>
						<h2>New Members</h2>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="stats1-info">
						<i class="icon-trophy"></i>
						<p><span class="count count1">187</span></p>
						<h2>Competitions</h2>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="stats1-info">
						<i class="icon-telescope"></i>
						<p><span class="count count1">923</span></p>
						<h2>Job Openings</h2>
					</div>
				</div>
			</div>
		</div>
		</div>

	-->

		<div class="clearfix space90"></div>

			<section id="portfolio-section">
			<div class="container container-full" id="3">
			<section id="portfolio-section">
<div class="container">
				<h3 class="uppercase text-center">Company</h3>
				<p class="text-center">Our Partner</p>
				<div class="clearfix"></div>
				<br><br>
			</div>
		<!--	<ul class="filter" data-option-key="filter">
					<li><a class="selected" href="#filter" data-option-value="*">All</a></li>
					<li><a href="#" data-option-value=".branding">Branding</a></li>
					<li><a href="#" data-option-value=".illustration">Illustration</a></li>
					<li><a href="#" data-option-value=".web-design">Web Design</a></li>
					<li><a href="#" data-option-value=".print">Print</a></li>
				</ul>-->
				<div id="portfolio-home" class="isotope folio-boxed-3col" style="position: relative; height: 867px;">
					<div class="project-item branding" style="position: absolute; left: 0px; top: 0px;">
						<a href="#">
							<div class="project-gal">
								<img src="../r/images/projects/1.jpg" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Aliquam tincidunt risus.</h2>
										<p>Web , Creative</p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item illustration web-design illustration" style="position: absolute; left: 449px; top: 0px;">
						<a href="#">
							<div class="project-gal">
								<img src="../r/images/projects/2.jpg" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Vestibulum auctor</h2>
										<p>Image Gallery</p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item illustration print" style="position: absolute; left: 899px; top: 0px;">
						<a href="#">
							<div class="project-gal">
								<img src="../r/images/projects/3.jpg" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Vestibulum auctor</h2>
										<p>Image Gallery</p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item web-design" style="position: absolute; left: 0px; top: 289px;">
						<a href="#">
							<div class="project-gal">
								<img src="../r/images/projects/4.jpg" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Vestibulum auctor</h2>
										<p>Image Gallery</p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item branding" style="position: absolute; left: 449px; top: 289px;">
						<a href="#">
							<div class="project-gal">
								<img src="../r/images/projects/5.jpg" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Vestibulum auctor</h2>
										<p>Image Gallery</p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item illustration web-design print" style="position: absolute; left: 899px; top: 289px;">
						<a href="#">
							<div class="project-gal">
								<img src="../r/images/projects/6.jpg" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Vestibulum auctor</h2>
										<p>Image Gallery</p>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</section>
		</div>
		</section>




<!--		<div class="pattern-grey padding70">
			<div class="container">
				<div class="row cta-dark">
					<div class="col-md-9">
						<h2 style="color: #000"><span>Welcome To Bluewater</span> - Business HTML Template</h2>
						<p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet purus quis metus imperdiet fermentum. Suspendisse hendrerit id lacus id lobortis. Vestibulum quam elit, dapibus ac augue ut.</p>
					</div>
					<div class="col-md-3">
						<a href="#" class="button btn-exlg pull-right">Download</a>
					</div>
				</div>
			</div>
		</div>-->

<!-- client-->


	<!--	<div class="clients container">
			<div id="carousel_five" class="owl-carousel">
				<div class="item client-logo">
					<a href="#"><img src="../r/images/clients/1.png" class="img-responsive" alt=""/></a>
				</div>
				<div class="item client-logo">
					<a href="#"><img src="../r/../r/../r/images/clients/2.png" class="img-responsive" alt=""/></a>
				</div>
				<div class="item client-logo">
					<a href="#"><img src="../r/../r/images/clients/3.png" class="img-responsive" alt=""/></a>
				</div>
				<div class="item client-logo">
					<a href="#"><img src="../r/images/clients/4.png" class="img-responsive" alt=""/></a>
				</div>
				<div class="item client-logo">
					<a href="#"><img src="../r/images/clients/5.png" class="img-responsive" alt=""/></a>
				</div>
				<div class="item client-logo">
					<a href="#"><img src="../r/images/clients/6.png" class="img-responsive" alt=""/></a>
				</div>
				<div class="item client-logo">
					<a href="#"><img src="../r/images/clients/7.png" class="img-responsive" alt=""/></a>
				</div>
			</div>
		</div>
				<div class="space80"></div>-->
<!--endofclient-->


		<div class="border-top padding80">
		<div class="container home-blog" id="5">
			<div class="text-center space40">
				<h2 class="title uppercase">Latest from blog</h2>
				<p>News - Promotion - Article<br></p>
			</div>

			<?php
		  // set up or arguments for our custom query
		  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		  $query_args = array(
		    'post_type' => 'post',
		    'category_name' => '',
		    'posts_per_page' => 3,
		    'paged' => $paged
		  );
		  // create a new instance of WP_Query
		  $the_query = new WP_Query( $query_args );
		?>

		<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>




			<div class="row">
				<div class="col-md-4">
					<div class="hb-info">
						<div class="hb-thumb">
							<?php

add_image_size( 'custox', 360, 196, true );
the_post_thumbnail('custox');

$ids=get_the_ID();
?>
<div class="date-meta">
								<?php the_date('Y-m-d'); ?>
							</div>

						</div>
						<h4><a href="blog_detail?id=<?php echo $ids; ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
					<p>	<?php the_excerpt(); ?></p>


					</div>
				</div>



<?php endwhile; ?>


<?php else: ?>
	 <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>



			</div>
		</div>
	</div>
<div class="clearfix"></div>
	<div class="border-top"></div>




				<div class="border-top"></div>
	<div class="container-fluid no-padding" id="7">
	<div>
			<div class="text-center">
				<h2 class="title uppercase">Contact Us</h2>
				<p>contact us if you have question or suggestion<br>Our Customer service, will respond you as soon as possible</p>
			</div>
						<div class="space40"></div>

		<div class="col-md-6 full-contact">


					<div class="space50"></div>
					<h3 class="no-margin">Contact info</h3>
					<div class="space20"></div>
					<ul class="contact-info">
						<li>
							<p><strong><i class="fa fa-map-marker"></i> Address:</strong> <?php echo $global_address; ?></p>
						</li>
						<li>
							<p><strong><i class="fa fa-envelope"></i> Mail Us:</strong><?php echo "<a href='mailto:$global_email'>$global_email</a>"; ?></p>
						</li>
						<li>
							<p><strong><i class="fa fa-phone"></i> Phone:</strong> <?php echo "<a href='tel:$global_phone'>$global_phone</a>"; ?></p>
						</li>
						<li>
							<p><strong><i class="fa fa-print"></i> Fax:</strong><?php echo $global_fax;?></p>
						</li>
					</ul>

		</div>
		<div class="col-md-6 no-padding">
			<div class="map-greyscale" id="map-greyscale"></div>
		</div>
	</div>

	</div>


		</div>


<?php
	include"footer.php";

?>
