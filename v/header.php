<?php
include"../b/wp-load.php";
include_once"../r/controller.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
	<title>Gili X-press</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">


	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="../r/css/bootstrap.min.css" type="text/css">

	<!-- Styles -->
	<link rel="stylesheet" href="../r/js/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../r/js/owl-carousel/owl.theme.css">
	<link rel="stylesheet" href="../r/js/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="../r/js/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="../r/js/flexslider/flexslider.css">
	<link rel="stylesheet" href="../r/js/isotope/isotope.css">
	<link rel="stylesheet" href="../r/css/jquery-ui.css">
	<link rel="stylesheet" href="../r/js/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../r/css/style.css">
		<link rel="stylesheet" href="../r/css/bootstrap-datepicker.min.css">

	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Dosis:400,200,300,500,600,700,800' rel='stylesheet' type='text/css'>

	<!-- Icon Fonts -->
	<link href="../r/css/icomoon/style.css" rel="stylesheet"  type="text/css">
	<link rel="stylesheet" href="../r/font-awesome/css/font-awesome.min.css" type="text/css">

	<!-- SKIN -->
	<link rel="stylesheet" href="../r/css/color-scheme/default-blue.css" type="text/css">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
           <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.2/jquery.xdomainrequest.min.js"></script>
	<![endif]-->

</head>
<body id="header6">

<div id="page-top"></div>
<div class="outer-wrapper">
	<div class="header-wrap">

		<!-- HEADER -->
		<header id="header-main">
		<div class="container">
			<div class="navbar yamm navbar-default">
				<div class="navbar-header">
					<button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a href="index.html" class="navbar-brand"><img src="../r/images/basic/logo.png" width="40" alt=""/></a>
				</div>

				<!-- SEARCH -->
				<div class="header-x pull-right">
					<div class="s-search">
						<div class="ss-trigger"><i class="icon-search2"></i></div>
						<div class="ss-content">
							<span class="ss-close icon-cross2"></span>
							<div class="ssc-inner">
								<form>
									<input type="text" placeholder="Type Search text here...">
									<button type="submit"><i class="icon-search2"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
					<ul class="nav navbar-nav">
						<li class="page-scroll"><a href="home">Home</a></li>
						<li class="page-scroll"><a href="#1">About Us</a></li>
						<li class="page-scroll"><a href="#2">Booking</a></li>
						<li class="page-scroll"><a href="#3">Destination</a></li>
						<li class="page-scroll"><a href="blog">Blog</a></li>
						<li class="page-scroll"><a href="#7">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	<div class="frontformz">

		<h3>Search Avability</h3>
			<form method="post" action="booking_step1">

			<div class="form-group">
				<div class="col-lg-6 col-md-6 col-sm-6">

				<select class="form-control" name="departure_place" placeholder="From" required>
					<option selected>Departure</option>
					<?php
					$qr=tampil_port();
					while($rw=mysql_fetch_array($qr))
					{
						echo "<option value='$rw[nama_port]'>$rw[nama_port]</option>";
					}
					?>
				</select>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<select class="form-control" name="arrival_place" placeholder="To" required>
					<option value="">Destination</option>
					<?php
					$qr=tampil_port();
					while($rw=mysql_fetch_array($qr))
					{
						echo "<option value='$rw[nama_port]'>$rw[nama_port]</option>";
					}
					?>
				</select>
			</div>
<div id="pertama" class="clearfix"></div>
			</div>

<div class="form-group">

<div class="col-md-6"></div><div class="col-md-6 chk"> <input type="checkbox" name="others" id="rControl" onclick="enable_text(this.checked)" class="medium" /> &nbsp;<span class="text-primary">Return</span></div>
</div>

			<div class="form-group">

			<div class="date col-md-6" data-provide="datepicker">

				<input class="form-control input-lg" type="text" name="departure_date" placeholder="Departure Date" required>
				<div class="input-group-addon">
        <span class="fa fa-calendar"></span>
    </div>
</div>
	<div class="date col-md-6" data-provide="datepicker">

				<input class="form-control input-lg" type="text" name="arrival_date" id="return" placeholder="Return Date" disabled="disabled" required>
				<div class="input-group-addon">
        <span class="fa fa-calendar"></span>
    </div>

</div>
<div id="pertama" class="clearfix"></div>
	</div>

			<div class="form-group">
				<div class="col-md-6"><input class="form-control input-sm" type="number" name="adult" placeholder="Adult" required></div><div class="col-md-6"><input class="form-control input-sm" type="number" name="child" placeholder="Child"></div>
<div id="pertama" class="clearfix"></div>
			</div>

			<div class="form-group">
				<input class="btn btn-primary" type="submit" value="Search">
				<div id="pertama" class="clearfix"></div>
			</div>
		</form>
	</div>
<script>

$('.datepicker').datepicker({

    startDate: '-3d'
});

</script>
	</div>
