<?php
include"header2.php";
?>

<!-- PAGE HEADER -->
<div class="page_header">
  <div class="page_header_parallax">
    <div class="container">
      <div class="row">
        <div class="col-md-12 head-bg">
          <h3>Step 3 - Confirm Your Booking </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="bcrumb-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="bcrumbs">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- INNER CONTENT -->
<?php


?>
<div class="inner-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <div class="blog-single">
          <article class="blogpost">
            <h2 class="post-title"><?php echo $title; ?></h2>

            <div class="space"></div>

            <!--start content -->



            <h4>Check All Your Booking Data</h4>
            <form method="post" action="booking_step4">

            <?php
          $cust_name=$_POST['cust_name'];
          $cust_address=$_POST['cust_address'];
          $cust_city=$_POST['cust_city'];
          $cust_country=$_POST['cust_country'];
          $cust_phone=$_POST['cust_phone'];
          $cust_email=$_POST['cust_email'];
          $cust_pickup=$_POST['pickup_status'];
          $cust_pickup_r=$_POST['pickup_status_r'];
          $cust_pickup_address=$_POST['alamat_pickup'];
          $cust_pickup_address_r=$_POST['alamat_pickup_r'];
          $cust_payment=$_POST['payment_option'];
          $cust_payment_id=$_POST['payment_acc'];

          $trip=$_POST['trip'];
          $r_trip=$_POST['r_trip'];
          $departure_date=$_POST['departure_date'];
          $arrival_date=$_POST['arrival_date'];
          $adult_amount=$_POST['adult_amount'];
          $child_amount=$_POST['child_amount'];

          $BX_NAME=$_POST['BX_NAME'];        // array
          $BX_age=$_POST['BX_age'];	   // array
          $BX_gender=$_POST['BX_gender'];    // array

          ?>
          <?php foreach($BX_NAME as $a => $b){ ?>

      			<input type="hidden" name="BX_NAME[]" value="<?php echo $BX_NAME[$a]; ?>">

      			<input type="hidden" class="small"  name="BX_age[]" value="<?php echo $BX_age[$a]; ?>">

      			<input type="hidden"  name="BX_gender[]" value="<?php echo $BX_gender[$a]; ?>">

      <?php } ?>
          <input type="hidden" name="trip" value="<?php echo $trip;?>">
          <input type="hidden" name="r_trip" value="<?php echo $r_trip;?>">
          <input type="hidden" name="departure_date" value="<?php echo $departure_date;?>">
          <input type="hidden" name="arrival_date" value="<?php echo $arrival_date;?>">
          <input type="hidden" name="adult_amount" value="<?php echo $adult_amount;?>">
          <input type="hidden" name="child_amount" value="<?php echo $child_amount;?>">
          <table class="table">
            <thead>
              <tr class="success">
                <td><b>Customer Details</b></td><td><i><b> <?php
                $kodok=kode_booking();
                $kode=kode_unik();?></b></i></td>
                <input type="hidden" name="booking_ident" value="<?php echo $kodok;?>">
                <input type="hidden" name="booking_code" value="<?php echo $kode;?>">
              </tr>

                 <?php $kode_r=kode_unik();?>
                <input type="hidden" name="booking_code_r" value="<?php echo $kode_r;?>">

            </thead>
  <tbody>
    <tr>
      <td>Customer Name</td>
      <td><?php echo $cust_name;?></td>
        <input type="hidden" name="cust_name" value="<?php echo $cust_name;?>">
    </tr>
    <tr >
      <td>Address</td>
      <td><?php echo $cust_address;?></td>
      <input type="hidden" name="cust_address" value="<?php echo $cust_address;?>">
    </tr>
    <tr>
      <td>City</td>
      <td><?php echo $cust_city;?></td>
      <input type="hidden" name="cust_city" value="<?php echo $cust_city;?>">
    </tr>
    <tr>
      <td>Country</td>
      <td><?php echo $cust_country;?></td>
<input type="hidden" name="cust_country" value="<?php echo $cust_country;?>">
    </tr>
    <tr>
      <td>Phone</td>
      <td><?php echo $cust_phone;?></td>
<input type="hidden" name="cust_phone" value="<?php echo $cust_phone;?>">
    </tr>
    <tr>
      <td>Email</td>
      <td><?php echo $cust_email;?></td>
<input type="hidden" name="cust_email" value="<?php echo $cust_email;?>">
    </tr>

      <input type="hidden" name="cust_pickup" value="<?php echo $cust_pickup;?>">
      <input type="hidden" name="cust_pickup_address" value="<?php echo $cust_pickup_address;?>">
      <input type="hidden" name="cust_pickup_r" value="<?php echo $cust_pickup_r;?>">
      <input type="hidden" name="cust_pickup_address_r" value="<?php echo $cust_pickup_address_r;?>">
  <tr>
       <td>Payment Type</td>
       <td><?php echo$cust_payment;?></td>
       <input type="hidden" name="cust_payment" value="<?php echo $cust_payment;?>">
     </tr>
     <tr>
       <td>Payment ID</td>
       <td><?php echo$cust_payment_id;?></td>
       <input type="hidden" name="cust_payment_id" value="<?php echo $cust_payment_id;?>">
     </tr>
  </tbody>
</table>
<br>
<table class="table">
  <caption>Passenger List</caption>
  <thead>
    <tr>
    <th>No</th>
    <th>Name</th>
    <th>Age</th>
    <th>Gender</th>
  </tr>
  </thead>
  <tbody>
<?php foreach($BX_NAME as $a => $b){ ?>
	<tr>

		<td>
			<?php echo $a+1; ?>
		</td>
		<td>

			<?php echo $BX_NAME[$a]; ?></label>
		</td>
		<td>

			<?php echo $BX_age[$a]; ?>
		</td>
		<td>

    <?php echo $BX_gender[$a]; ?>
		</td>

	</tr>
<?php } ?>
</tbody>
</table>






<!-- booking detail -->
<?php echo "<h3> Booking ID ".$kodok."</h3>" ?>
<br>
<h3>DEPART</h3>
<?php
$x=tampil_harga($trip);
$xr=mysql_fetch_array($x);
?>
<table class="table table-hover">
<tbody>
<tr>
<td>Date</td><td>:&nbsp;<?php echo $departure_date;?></td><td>Estimated Time Departure</td><td>:&nbsp;<?php echo $xr['keberangkatan'];?></td></tr>
<tr><td><!--Booking Code--></td><td><!--:&nbsp;<b><?php echo $kode; ?></b>--></td><td>Estimated Time Arrival</td><td>:&nbsp;<?php echo $xr['kedatangan'] ?></td>
</tr>
<tr>
<td>Pickup Address</td><td>:&nbsp;<?php echo $cust_pickup_address;?></td><td>Boat Name</td>
<td>:&nbsp;
<?php   $z=tampil_ref_boat_jadwal($trip);
$zr=mysql_fetch_array($z);
echo $zr['nama_boat'];
?>
</td>
</tr>
<tr>
  <td>Fare Details</td>
  <td>
    Passenger amount<br>
    <?php echo $adult_amount."&nbsp;Adult"; ?>
    <br>
    <?php echo $child_amount."&nbsp;Child"; ?>
  </td>
<td>
  Ticket Price<br>
  <?php
echo "Rp. ".$xr['tarif_dewasa'];
echo "<br>";
echo "Rp. ".$xr['tarif_anak'];
?></td>
<td>
  <text class="pull-right">
  Sub totals<br>
<?php
    $subtotal_trip=$adult_amount*$xr['tarif_dewasa'];
    echo "Rp. ".$subtotal_trip;
?></text>
<br><br>
<text class="pull-right"><?php
    $c_subtotal_trip=$child_amount*$xr['tarif_anak'];
    echo "Rp. ".$c_subtotal_trip;
?></text>
</td>
</tr>
</tbody>
</table>

<?php if($r_trip!=="")
{
?>
<h3>RETURN</h3>



<?php
$y=tampil_harga($r_trip);
$yr=mysql_fetch_array($y);
?>
<table class="table table-hover">
<tbody>
<tr>
<td>Date</td><td>:&nbsp;<?php echo $arrival_date;?></td><td>Estimated Time Departure</td><td>:&nbsp;<?php echo $yr['keberangkatan'];?></td></tr>
<tr><td><!--Booking Code--></td><td><!--:&nbsp;<b><?php echo $kode_r; ?>--></b></td><td>Estimated Time Arrival</td><td>:&nbsp;<?php echo $yr['kedatangan'] ?></td>
</tr>
<tr>
<td>Pickup Address</td><td>:&nbsp;<?php echo $cust_pickup_address_r;?></td><td>Boat Name</td>
<td>:&nbsp;
<?php   $w=tampil_ref_boat_jadwal($r_trip);
$zr=mysql_fetch_array($w);
echo $zr['nama_boat'];
?>
</td>
</tr>
<tr>
  <td>Fare Details</td>
  <td>
    Passenger amount<br>
    <?php echo $adult_amount."&nbsp;Adult"; ?>
    <br>
    <?php echo $child_amount."&nbsp;Child"; ?>
  </td>
<td>
  Ticket Price<br>
  <?php
echo "Rp. ".$yr['tarif_dewasa'];
echo "<br>";
echo "Rp. ".$yr['tarif_anak'];
?></td>
<td>
  <text class="pull-right">
  Sub totals<br>
  <?php
  $subtotal_r_trip=$adult_amount*$yr['tarif_dewasa'];
  echo "Rp. ".$subtotal_r_trip;
  ?></text>
<br><br>
<text class="pull-right"><?php
    $c_subtotal_r_trip=$child_amount*$yr['tarif_anak'];
    echo "Rp. ".$c_subtotal_r_trip;
?></text>
</td>
</tr>
</tbody>
</table>

<?php
};
?>

<br>
<table class="table">
  <tbody><tr class="primary">
<td>Grand Total</td>
<td>
<text class="pull-right"> <b> <?php
$grandtotal=$subtotal_trip+$subtotal_r_trip+$c_subtotal_trip+$c_subtotal_r_trip;
echo"Rp. ".$grandtotal;
?></b></text>
</td>
</tr>
</tbody>
</table>






          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-md">Confirm</button>
          </div>
          </form>
          <!--- end of content -->
          </article>
        </div>
        <div class="padding70">

        </div>


        <div class="space60"></div>
        <div class="clearfix prevnext">

        </div>
      </div>
    </div>
  </div>
</div>





<?php include"footer.php"?>
