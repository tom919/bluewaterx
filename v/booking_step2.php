<?php
include"header2.php";
?>

<!-- PAGE HEADER -->
<div class="page_header">
  <div class="page_header_parallax">
    <div class="container">
      <div class="row">
        <div class="col-md-12 head-bg">
          <h3>Step 2 - Fill Your Details</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="bcrumb-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="bcrumbs">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- INNER CONTENT -->
<?php


?>
<div class="inner-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <div class="blog-single">
          <article class="blogpost">
            <h2 class="post-title"><?php echo $title; ?></h2>

            <div class="space"></div>


            <?php
          $trip=$_POST['trip'];
          $r_trip=$_POST['r_trip'];
          $departure_date=$_POST['departure_date'];
          $arrival_date=$_POST['arrival_date'];
          $adult_amount=$_POST['adult_amount'];
          $child_amount=$_POST['child_amount'];

          ?>

            <!--start content -->




            <form method="post" action="booking_step3">
              <div class="form-group">
                <input type="text" name="cust_name" class="form-control" placeholder="Name" required>
              </div>
              <div class="form-group">
                <textarea name="cust_address" class="form-control" rows="4" cols="50" placeholder="Address" required></textarea>
              </div>
              <div class="form-group">
                <input type="text" name="cust_city" class="form-control" placeholder="City" required>
              </div>
              <div class="form-group">
                <input type="text" name="cust_country" class="form-control" placeholder="Country" required>
              </div>
              <div class="form-group">
                <input type="tel" name="cust_phone" class="form-control" placeholder="Phone Number" required>
              </div>
              <div class="form-group">
                <input type="email" name="cust_email" class="form-control" placeholder="Email" required>
              </div>
               <input type="checkbox" name="pickup_status" id="rControl2" onclick="enable_text(this.checked)" class="medium" value="1"/> &nbsp;<span class="text-primary">Pickup</span>
              <div class="form-group">
                <textarea name="alamat_pickup" id="pickup" class="form-control" rows="4" cols="50" placeholder="Pickup Address" disabled="disabled" required></textarea>
              </div>
              <?php if($r_trip!=""){
                ?>
              <input type="checkbox" name="pickup_status_r" id="rControl3" onclick="enable_text(this.checked)" class="medium" value="1"/> &nbsp;<span class="text-primary">Return Pickup</span>
              <div class="form-group">
                <textarea name="alamat_pickup_r" id="pickup_r" class="form-control" rows="4" cols="50" placeholder="Return Pickup Address" disabled="disabled" required></textarea>
              </div>
              <?php } ?>



           <!-- table-->
          <table class="table table-hover">
  <thead>
    <tr><td><h4>Trip Details</h4></td></tr>
    <tr>
      <td>Date</td>
      <td>Trip Status</td>
      <td>Boat</td>
      <td>Passenger Amount</td>
      <td>Ticket Price</td>
      <td>Total</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $departure_date;?></td>
      <td>Trip</td>
      <td>
        <?php   $z=tampil_ref_boat_jadwal($trip);
          $zr=mysql_fetch_array($z);
          echo $zr['nama_boat'];
          ?>
        </td>
      <td>Adult amount :<?php echo $adult_amount; ?><br>Children amount :<?php echo $child_amount;?></td>
      <td><?php   $x=tampil_harga($trip);
        $xr=mysql_fetch_array($x);
        echo "Rp. ".$xr['tarif_dewasa'];
        echo "<br>";
        echo "Rp. ".$xr['tarif_anak'];
        ?></td>
      <td>
          <text class="pull-right"><?php
              $subtotal_trip=$adult_amount*$xr['tarif_dewasa'];
              echo "Rp. ".$subtotal_trip;
          ?></text>
          <br>
          <text class="pull-right"><?php
              $c_subtotal_trip=$child_amount*$xr['tarif_anak'];
              echo "Rp. ".$c_subtotal_trip;
          ?></text>
        </td>
    </tr>
    <?php if($r_trip!=""){
      ?>
    <tr>
      <td><?php echo $arrival_date?></td>
      <td><?php if($r_trip!=""){
        echo"Return Trip";} ?></td>
      <td>
        <?php   $z=tampil_ref_boat_jadwal($r_trip);
          $zr=mysql_fetch_array($z);
          echo $zr['nama_boat'];
          ?>
        </td>
      <td><?php if($r_trip!==""){
        echo"Adult amount"; echo $adult_amount; echo"<br>Child amount"; echo $child_amount; }?></td>
      <td><?php   $z=tampil_harga($r_trip);
        $yr=mysql_fetch_array($z);
        echo "Rp. ".$yr['tarif_dewasa'];
        echo "<br>";
        echo "Rp. ".$yr['tarif_anak'];
        ?></td>
      <td><text class="pull-right"><?php
          $subtotal_r_trip=$adult_amount*$yr['tarif_dewasa'];
          echo "Rp. ".$subtotal_r_trip;
          ?></text>
          <br>
          <text class="pull-right"><?php
              $c_subtotal_r_trip=$child_amount*$yr['tarif_anak'];
              echo "Rp. ".$c_subtotal_r_trip;
          ?></text>

      </td>
    </tr> <?php } ?>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Total</td>
      <td>
      <text class="pull-right">  <?php
        $grandtotal=$subtotal_trip+$subtotal_r_trip+$c_subtotal_trip+$c_subtotal_r_trip;
        echo"Rp. ".$grandtotal;
        ?></text>
      </td>
    </tr>
  </tbody>
</table>





          <input type="hidden" value="<?php echo $trip;?>" name="trip">
          <input type="hidden" value="<?php echo $r_trip; ?>" name="r_trip">
          <input type="hidden" value="<?php echo $departure_date; ?>" name="departure_date">
          <input type="hidden" value="<?php echo $arrival_date;?>" name="arrival_date">
          <input type="hidden" value="<?php echo $adult_amount; ?>" name="adult_amount">
          <input type="hidden" value="<?php echo $child_amount; ?>" name="child_amount">


          <div class="form-group">
        <select name="payment_option" class="form-control" required>
          <option selected="">Choose Payment</option>
          <option value="banktransfer">Bank Transfer</option>
          <option value="paypal">Paypal</option>

        </select>
      </div>
      <div class="form-group">
        <input type="text" name="payment_acc" class="form-control" placeholder="Payment Account ID" required>
      </div>


      <div class="form-group">
        <fieldset class="row2">
				<legend>Passenger Details</legend>
				<p>
					<input type="button" value="Add Passenger" onClick="addRow('dataTable')" class="btn"/>
					<input type="button" value="Remove Passenger" onClick="deleteRow('dataTable')" class="btn"  />

				</p>
               <table id="dataTable" class="table form" border="1">
                  <tbody>
                    <tr>
                      <p>
						<td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
						<td>
							<label>Name</label>
							<input type="text" required="required" name="BX_NAME[]">
						 </td>
						 <td>
							<label for="BX_age">Age</label>
							<input type="text" required="required" class="small"  name="BX_age[]">
					     </td>
						 <td>
							<label for="BX_gender">Gender</label>
							<select id="BX_gender" name="BX_gender[]" required="required">
								<option></option>
								<option>Male</option>
								<option>Female</option>
							</select>
						 </td>

							</p>
                    </tr>
                    </tbody>
                </table>
				<div class="clear"></div>
            </fieldset>

      </div>


          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-md">Continue</button>
          </div>
          </form>
          <!--- end of content -->
          </article>
        </div>
        <div class="padding70">

        </div>


        <div class="space60"></div>
        <div class="clearfix prevnext">

        </div>
      </div>
    </div>
  </div>
</div>





<?php include"footer.php"?>
