<?php
include"header2.php";
?>

<!-- PAGE HEADER -->
<div class="page_header">
  <div class="page_header_parallax">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3>Blog</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="bcrumb-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="bcrumbs">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- INNER CONTENT -->
<?php
  $idf=$_GET['id'];
$post = get_post($idf);
$title = apply_filters('the_title', $post->post_title);
$content = apply_filters('the_content', $post->post_content);
$author = apply_filters('the_author',$post->post_author);
$date= date("Y-m-d", strtotime($post->post_date));
?>
<div class="inner-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <div class="blog-single">
          <article class="blogpost">
            <h2 class="post-title"><?php echo $title; ?></h2>
            <div class="post-meta">
              <span><a href="#"><i class="icon-clock2"></i> <?php echo $date; ?></a></span>
            <!--  <span><a href="#"><i class="icon-user"></i> </a></span>-->
            </div>
            <div class="space30"></div>
            <!-- Media Gallery -->
            <div class="post-media">
              <div id="blog-slider" class="owl-carousel owl-theme">
                <div class="item">
                <!---featured - images -->
                <?php echo get_the_post_thumbnail($page_for_posts, 'large'); ?>

                <!---  end of featured images -->
              </div>
            </div>
            <div class="space30"></div>
            <!--start content -->
            <?php echo $content; ?>
          <!--- end of content -->
          </article>
        </div>
        <div class="padding70">

        </div>


        <div class="space60"></div>
        <div class="clearfix prevnext">

        </div>
      </div>
    </div>
  </div>
</div>





<?php include"footer.php"?>
