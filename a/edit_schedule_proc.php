<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Jadwal
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Jadwal</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Jadwal</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php

          $id=$_POST['id'];
          $id_kapal=$_POST['id_kapal'];
          $waktu_berangkat=$_POST['waktu_berangkat'];
          $waktu_datang=$_POST['waktu_datang'];
          $port_berangkat=$_POST['port_berangkat'];
          $port_datang=$_POST['port_datang'];
          $jenis_jadwal=$_POST['jenis_jadwal'];
          $tarif_dewasa=$_POST['tarif_dewasa'];
          $tarif_anak=$_POST['tarif_anak'];


            $xst=edit_jadwal($id,$id_kapal,$waktu_berangkat,$waktu_datang,$port_berangkat,$port_datang,$jenis_jadwal,$tarif_dewasa,$tarif_anak);
            if($xst==10){

            echo "<div class='alert alert-success'>

                <h4><i class='icon fa fa-check'></i> Berhasil!</h4>
                Jadwal berhasil diedit
                <a href='main_schedule'><button class='btn btn-default pull-right'><i class='fa fa-arrow-left'></i>&nbsp;Kembali</button></a>
                <div class='clearfix'></div>
              </div>
            </div>";
            }else{

             echo "error level 1";

            }


            ?>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
