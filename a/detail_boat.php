
   <div class="modal-dialog modal-md">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Detail Perusahaan</h4>
       </div>
       <div class="modal-body">
         <?php
         include_once"../r/admin_controller.php";
         $id=$_GET['id'];
         $q=pilih_boat($id);
         $row=mysql_fetch_array($q);

         ?>
         <form role="form" method="post" action="edit_boat_proc">
           <div class="box-body">
             <div class="form-group">
               <label>Kode Boat</label>
               <h3><?php echo $row['id_boat']; ?></h3>



             </div>
             <div class="form-group">
               <label>Nama Boat</label>
               <?php echo $row['nama_boat']; ?>

             </div>
             <div class="form-group">
               <label>Kapasitas</label>
               <?php echo $row['kapasitas']; ?>

             </div>
             <div class="form-group">
               <label>Perusahaan Pemilik</label>
               <?php $term=$row['ref_per'];
               $query=cari_ref_per($term);
               $row=mysql_fetch_array($query);
               echo $row['nama_perusahaan'];
                ?>

             </div>

           <!-- /.box-body -->


         </form>




       </div>
       <div class="modal-footer">

         <a href="edit_boat?id=<?php echo $row['id_boat'];?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button></a>
         <a href="delete_boat?id=<?php echo $row['id_boat'];?>"><button type="button" class="btn btn-warning"><i class="fa fa-trash"></i>&nbsp;Hapus</button></a>
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>

   </div>
   <script>
   $('.ls-modal').on('click', function(e){
     e.preventDefault();
     $('#myModal').modal('show').find('.modal-body').load($(this).attr('href'));
   });
</script>
   <script>
   $("#myModal").on('hidden.bs.modal', function () {
       $(this).data('bs.modal', null);
   });
   </script>
