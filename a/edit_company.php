<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Perusahaan Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Perusahaan</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Perusahaan</h3>
            </div>
            <!-- /.box-header -->
            <?php
            $id=$_GET['id'];
            $q=pilih_perusahaan($id);
            $row=mysql_fetch_array($q);

            ?>

            <!-- form start -->
            <form role="form" method="post" action="edit_company_proc">
              <div class="box-body">
                <div class="form-group">
                  <label>Kode Perusahaan</label>
                  <h3><?php echo $row['id_per']; ?></h3>
                  <input type="hidden" class="form-control" id="kode_perusahaan" name="kode_perusahaan" value="<?php echo $row['id_per']; ?>" required>

                </div>
                <div class="form-group">
                  <label>Nama Perusahaan</label>
                  <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="<?php echo $row['nama_perusahaan']; ?>" required>
                </div>
								<div class="form-group">
                  <label>Alamat</label>
									<textarea class="form-control" id="alamat" name="alamat" required><?php echo $row['alamat']; ?></textarea>
								</div>
								<div class="form-group">
                  <label>Telp</label>
									<input type="tel" class="form-control" id="phone" name="phone" value="<?php echo $row['telp']; ?>" required>
								</div>
								<div class="form-group">
                  <label>Email</label>
									<input type="email" class="form-control" id="email" name="email" value="<?php echo $row['email']; ?>" required>
								</div>
              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
