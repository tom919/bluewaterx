<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Booking
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Booking</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">



            <!-- /.box-header -->
            <div class='alert alert-warning'>
<?php
  $id=$_GET['id'];
  $idc=$_GET['idc']
?>
                        <h4><i class='icon fa fa-warning'></i> Konfirmasi Penghapusan</h4>
                        Anda yakin akan menghapus Booking dengan Nomor <?php echo $id ?>? &nbsp;
                        <div>
                          <br>
                          <p class="pull-right">
                        <a href='delete_booking_proc?id=<?php echo $id?>&idc=<?php echo $idc; ?>'><button type='button'  class='btn btn-danger'><i class='fa fa-trash-o'></i>&nbsp;Hapus</button></a>
                       &nbsp;&nbsp;&nbsp;

                        <a href='main_booking'><button type='button' class='btn btn-success'><i class='fa fa-ban'></i>&nbsp;Batal</button></a>
                      </p>
                      </div>

                        <div class='clearfix'></div>
                      </div>
                    </div>";
          </div>

<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
