<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Slider
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Slider</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Slider</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="add_slider_proc" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label>Ukuran Slide 1616x766 px, Max 1 MB</label>
                  <input type="file" class="form-control" name="foto" required>
                </div>
                <div class="form-group">

                  <input type="text" class="form-control" name="caption" placeholder="caption">
                </div>
                <div class="form-group">

									<input type="text" class="form-control" name="content" placeholder="small content">
								</div>
                <div class="form-group">

                  <input type="text" class="form-control" name="link_title" placeholder="Link Title">
                </div>

								<div class="form-group">
                  <label>contoh : http://linkaddress.com</label>
									<input type="text" class="form-control" name="link" placeholder="Link">
								</div>

              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambahkan</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
