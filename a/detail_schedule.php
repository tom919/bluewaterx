
   <div class="modal-dialog modal-md">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Detail Jadwal</h4>
       </div>
       <div class="modal-body">
         <?php
         include_once"../r/admin_controller.php";
         $id=$_GET['id'];
         $q=pilih_jadwal($id);
         $row=mysql_fetch_array($q);

         ?>

           <div class="box-body">



           <!-- /.box-body -->
           <table class="table">
  <thead>
    <tr>
      <th>Id Jadwal</th>
      <th></th>
      <th><?php echo $row['id']; ?></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Nama Boat</td>
      <td></td>
      <td>       <?php

             $q=tampil_ref_boat($row['id_kapal']);
             $r=mysql_fetch_array($q);
             echo $r['nama_boat'];
             ?>
           </td>
    </tr>
    <tr class="success">
      <td>Waktu Keberangkatan</td>
      <td></td>
      <td><?php echo $row['keberangkatan']; ?></td>
    </tr>
    <tr class="danger">
      <td>Waktu Kedatangan</td>
      <td></td>
      <td><?php echo $row['kedatangan']; ?></td>
    </tr>
    <tr class="info">
      <td>Pelabuhan Keberangkatan</td>
      <td></td>
      <td> <?php echo $row['port_keberangkatan']; ?></td>
    </tr>
    <tr class="warning">
      <td>Pelabuhan Kedatangan</td>
      <td></td>
      <td> <?php echo $row['port_kedatangan']; ?></td>
    </tr>
    <tr class="active">
      <td>Jenis Jadwal</td>
      <td></td>
      <td> <?php echo $row['jenis']; ?></td>
    </tr>
    <tr class="primary">
      <td>Tarif</td>
      <td></td>
      <td> Dewasa : <?php echo "Rp. ".number_format($row['tarif_dewasa'],2,",","."); ?> <br> Anak-anak : <?php echo "Rp. ".number_format($row['tarif_anak'],2,",","."); ?></td>
    </tr>
  </tbody>
</table>


       </div>


       <div class="modal-footer">

         <a href="edit_schedule?id=<?php echo $row['id'];?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button></a>
         <a href="delete_schedule?id=<?php echo $row['id'];?>"><button type="button" class="btn btn-warning"><i class="fa fa-trash"></i>&nbsp;Hapus</button></a>
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>

   </div>
   <script>
   $('.ls-modal').on('click', function(e){
     e.preventDefault();
     $('#myModal').modal('show').find('.modal-body').load($(this).attr('href'));
   });
</script>
   <script>
   $("#myModal").on('hidden.bs.modal', function () {
       $(this).data('bs.modal', null);
   });
   </script>
