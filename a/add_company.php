<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Perusahaan Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Perusahaan</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Perusahaan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="add_company_proc">
              <div class="box-body">
                <div class="form-group">

                  <input type="text" class="form-control" id="kode_perusahaan" name="kode_perusahaan" placeholder="Kode Perusahaan" required>
                </div>
                <div class="form-group">

                  <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" required>
                </div>
								<div class="form-group">

									<textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat" required></textarea>
								</div>
								<div class="form-group">

									<input type="tel" class="form-control" id="phone" name="phone" placeholder="Telephone" required>
								</div>
								<div class="form-group">

									<input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
								</div>
              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambahkan</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
