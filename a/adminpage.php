<?php
include "header.php";
 ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-ship"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">boat tersedia</span>
              <span class="info-box-number">10</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-plus-square"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Booking baru</span>
              <span class="info-box-number"><?php echo hitung_booking_terbaru(); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">


            <span class="info-box-icon bg-yellow"><i class="fa fa-flag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Booking Pending</span>
              <span class="info-box-number"><?php echo hitung_booking_unconfirmed(); ?></span>
            </div>


            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">

            <span class="info-box-icon bg-green"><i class="fa fa-check-square-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Booking terkonfirm</span>
              <span class="info-box-number"><?php echo hitung_booking_confirmed();?></span>
            </div>



            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<div class="row">
<div class="col-md-12">
  <!-- TABLE: LATEST ORDERS -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Booking terbaru</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
          <tr>
            <th>Booking ID</th>
            <th>Nama Customer</th>
            <th>Negara</th>
            <th>Phone</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            <?php
            $q=tampil_booking_terbaru();
            while($qr=mysql_fetch_array($q))
            {
            ?>
          <tr>
            <td><a href="pages/examples/invoice.html"><?php echo $qr['kode_booking'];?></a></td>
            <td><?php
            $q1=tampil_ref_customer($qr['id_customer']);
            $q1r=mysql_fetch_array($q1);
            echo"<a href='detail_customer.php?id=$qr[id_customer]'>";
            echo $q1r['nama_customer'];
            echo"</a>";
            ?></td>
            <td><?php echo $q1r['negara'];?></td>
            <td>
              <?php echo $q1r['telp'];?>
            </td>
            <td><span class="label label-<?php
              switch($qr['status']){
                case "unconfirmed":
                  echo "warning";
                  break;
                case "paid":
                  echo "success";
                  break;
                case "cancel":
                  echo "danger";
                  break;
              }

            ?>"><?php echo $qr['status'];?></span></td>
            <td>
              <div class="btn-group">
                   <button type="button" class="btn btn-info">Action</button>
                   <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                     <span class="caret"></span>
                     <span class="sr-only">Toggle Dropdown</span>
                   </button>
                   <ul class="dropdown-menu" role="menu">
                     <li><a href="detail_booking?id=<?php echo $qr['id_booking']?>" data-toggle="modal" data-target="#myModal">Detail</a></li>
                      <li><a href="edit_paid?id=<?php echo $qr['id_booking']?>">Konfirm Bayar</a></li>
                     <li><a href="edit_booking?id=<?php echo $qr['id_booking']?>">Update Booking</a></li>
                     <li><a href="delete_booking?id=<?php echo $qr['id_booking']?>&idc=<?php echo $qr['id_customer']?>">Hapus</a></li>
                   </ul>
                 </div>
            </td>
          </tr>
          <?php }; ?>




          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">

      <a href="main_booking" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua</a>
    </div>
    <!-- /.box-footer -->
  </div>
</div>


</div>

      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-4">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Cari Booking</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-tools margin">
          <?php
            modul_cari_booking();
          ?>
          <br>
          <br>
            </div>
          </div>
          <!-- /.box -->

          <!-- /.row -->


          <!-- /.box -->
        </div>




        <div class="col-md-4">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Cari Boat</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-tools margin">
          <?php
            modul_cari_boat();
          ?>
          <br>
          <br>
            </div>
          </div>
          <!-- /.box -->

          <!-- /.row -->


          <!-- /.box -->
        </div>


        <!-- /.col -->

        <div class="col-md-4">
          <!-- Info Boxes Style 2 -->

          <!-- /.info-box -->
      <!--    <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

      <      <div class="info-box-content">
              <span class="info-box-text">Kepuasan Customer</span>
              <span class="info-box-number">92</span>

              <div class="progress">
                <div class="progress-bar" style="width: 20%"></div>
              </div>
                  <span class="progress-description">
                    20%
                  </span>
            </div>-->
            <!-- /.info-box-content -->
      <!--    </div>-->
          <!-- /.info-box -->
  <!--        <div class="info-box bg-black">
            <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pembatalan / Refund</span>
              <span class="info-box-number">1</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70%
                  </span>
            </div>-->
            <!-- /.info-box-content -->
          <!---</div>-->
          <!-- /.info-box -->
      <!--    <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Jumlah pengunjung</span>
              <span class="info-box-number">163</span>

              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
                  <span class="progress-description">

                  </span>
            </div>-->
            <!-- /.info-box-content -->
        <!--  </div>-->
          <!-- /.info-box -->


          <!-- /.box -->

          <!-- PRODUCT LIST -->

          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->
   <div class="modal fade" id="myModal" role="dialog">
     <div class="modal-dialog">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Detail Booking</h4>
         </div>
         <div class="modal-body">

         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>

     </div>

   </div>
<?php include'footer.php';?>
