<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Profil
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Profil</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update Profil</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php

          $id=$_POST['id'];
          $password=$_POST['password'];

            $xst=update_password($id,$password);
            if($xst==10){

            echo "<div class='alert alert-success'>

                <h4><i class='icon fa fa-check'></i> Berhasil!</h4>
                Profil berhasil di Update
                <a href='adminpage'><button class='btn btn-default pull-right'><i class='fa fa-arrow-left'></i>&nbsp;Kembali</button></a>
                <div class='clearfix'></div>
              </div>
            </div>";
            }else{

             echo "error level 1";

            }


            ?>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
