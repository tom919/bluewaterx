<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Boat</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Boat</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="add_boat_proc">
              <div class="box-body">
                <div class="form-group">

                  <input type="text" class="form-control" id="kode_boat" name="kode_boat" placeholder="Kode Boat" required>
                </div>
                <div class="form-group">

                  <input type="text" class="form-control" id="nama_boat" name="nama_boat" placeholder="Nama Boat" required>
                </div>
								<div class="form-group">

									<input type="number" class="form-control" id="kapasitas" name="kapasitas" placeholder="Kapasitas" required>
								</div>
								<div class="form-group">

									<select name="ref_per" class="form-control" id="ref_per" required>
                    <option value="" selected></option>
                    <?php
                    $query=referensi_per();
                    while($row=mysql_fetch_array($query)){

                     ?>
                     <option value="<?php echo $row['id_per'];?>"><?php echo $row['nama_perusahaan'];?></option>


                     <?php }?>

                  </select>
								</div>

              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambahkan</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
