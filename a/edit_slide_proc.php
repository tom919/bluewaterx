<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Perusahaan Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Perusahaan</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Perusahaan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
            $id=$_POST['id'];
            $file=$_FILES['foto']['tmp_name'];
            $caption=$_POST['caption'];
            $link=$_POST['link'];
            $content=$_POST['content'];
            $link_title=$_POST['link_title'];
            $fotoid=$_POST['fotoid'];

            if (empty($file)) {

              $set_cond="caption='$caption',content='$content',link_title='$link_title',link='$link'";
              $xst=edit_slide($id,$set_cond);
              if($xst==10){

              echo "<div class='alert alert-success'>

                  <h4><i class='icon fa fa-check'></i> Berhasil!</h4>
                  Slide berhasil diedit
                  <a href='main_slider'><button class='btn btn-default pull-right'><i class='fa fa-arrow-left'></i>&nbsp;Kembali</button></a>
                  <div class='clearfix'></div>
                </div>
              </div>";
              }else{

               echo "error level 1";

              }



	}else{
    $image= addslashes(file_get_contents($_FILES['foto']['tmp_name']));
  	$image_name="img"."-"."slide"."-".time();
  	$image_size= getimagesize($_FILES['foto']['tmp_name']);




    move_uploaded_file($_FILES["foto"]["tmp_name"],"../r/images/slider/" . $image_name.".jpg");
    $foto=$image_name;
			unlink('../r/images/slider/'.$fotoid.".jpg");
      $set_cond="foto='$foto',caption='$caption',content='$content',link_title='$link_title',link='$link'";
      $xst=edit_slide($id,$set_cond);
      if($xst==10){

      echo "<div class='alert alert-success'>

          <h4><i class='icon fa fa-check'></i> Berhasil!</h4>
          Slide berhasil diedit
          <a href='main_slider'><button class='btn btn-default pull-right'><i class='fa fa-arrow-left'></i>&nbsp;Kembali</button></a>
          <div class='clearfix'></div>
        </div>
      </div>";
      }else{

       echo "error level 1";

      }


			};







            ?>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
