
   <div class="modal-dialog modal-md">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Detail Booking</h4>
       </div>
       <div class="modal-body">
         <?php
         include_once"../r/admin_controller.php";
         $id=$_GET['id'];
         $q=pilih_booking($id);
         $row=mysql_fetch_array($q);

         ?>

           <div class="box-body">



           <!-- /.box-body -->
           <table class="table">
  <thead>
    <tr>
      <th>Kode Booking</th>
      <th></th>
      <th><?php echo $row['kode_booking']; ?></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Kode Unik</td>
      <td></td>
      <td>

            <?php echo $row['kode_unik']; ?>

           </td>
    </tr>
    <tr>
      <td>Nama Customer</td>
      <td></td>
      <td>

            <?php echo $row['nama_customer']; ?>

           </td>
    </tr>
    <tr>
      <td>Negara</td>
      <td></td>
      <td>

            <?php echo $row['negara']; ?>

           </td>
    </tr>
    <tr class="success">
      <td>Tanggal Booking</td>
      <td></td>
      <td><?php echo $row['tanggal']; ?></td>
    </tr>
    <tr class="danger">
      <td>Tanggal Berangkat</td>
      <td></td>
      <td><?php echo $row['tanggal_berangkat']; ?></td>
    </tr>
    <tr class="danger">
      <td>Keberangkatan</td>
      <td></td>
      <td><?php echo $row['port_keberangkatan']; ?></td>
    </tr>
    <tr class="danger">
      <td>Kedatangan</td>
      <td></td>
      <td><?php echo $row['port_kedatangan']; ?></td>
    </tr>
    <tr class="info">
      <td>Opsi Pickup</td>
      <td></td>
      <td> <?php if($row['opsi_pickup']=1)
        {

        echo"Yes";
      }
      else {
        echo "No";
      }
       ?></td>
    </tr>
    <tr class="warning">
      <td>Alamat Pickup</td>
      <td></td>
      <td> <?php echo $row['alamat_pickup']; ?></td>
    </tr>
    <tr class="warning">
      <td>Jenis pembayaran</td>
      <td></td>
      <td> <?php echo $row['payment_type']; ?></td>
    </tr>
    <tr class="active">
      <td>Status</td>
      <td></td>
      <td> <?php echo $row['status']; ?></td>
    </tr>
  </tbody>
</table><br>
<table class="table">
  <caption>Daftar Penumpang </caption>
<thead>
<tr>
<th>Nama</th>
<th>Jenis Kelamin</th>
<th>Umur</th>
</tr>
</thead>
<tbody>
  <?php
  $xc=tampil_penumpang($row['id_booking']);
  while($xcr=mysql_fetch_array($xc)){


  ?>
<tr>
<td><?php echo $xcr['nama_penumpang'];  ?></td>
<td><?php echo $xcr['jenis_kelamin']; ?></td>
<td><?php echo $xcr['umur']; ?></td>
</tr>
<?php } ?>
</tbody>
</table>

<br>
<table class="table">
  <caption>Detail Pembayaran </caption>
<thead>
<tr>
<th>Penumpang</th>
<th>Harga Tiket</th>
<th>Jumlah</th>
</tr>
</thead>
<tbody>
<?php $as=tampil_pembayaran($row['id_booking']);
  while($asr=mysql_fetch_array($as)){
?>
<tr>
<td><?php echo $asr['adult_amount']; ?>&nbsp;Dewasa</td>
<td><?php echo $asr['adult_ticket_price']; ?></td>
<td><?php
  $sub_adult=$asr['adult_amount']*$asr['adult_ticket_price'];;
 echo "Rp. &nbsp;". $sub_adult; ?></td>
</tr>
<tr>
<td><?php echo $asr['child_amount']; ?>&nbsp;Anak-anak</td>
<td><?php echo $asr['child_ticket_price']?></td>
<td><?php  $sub_child=$asr['child_amount']*$asr['child_ticket_price'];
 echo "Rp. &nbsp;".$sub_child; ?></td>
</tr>
<tr><td></td><td>Total</td><td><?php $grandtotal=$sub_adult+$sub_child; echo "Rp. &nbsp;".$grandtotal ?></td></tr>
<?php } ?>
</tbody>
</table>

       </div>


       <div class="modal-footer">

         <a href="edit_booking?id=<?php echo $row['id_booking'];?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button></a>
         <a href="delete_booking?id=<?php echo $row['id_booking'];?>"><button type="button" class="btn btn-warning"><i class="fa fa-trash"></i>&nbsp;Hapus</button></a>
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>

   </div>
   <script>
   $('.ls-modal').on('click', function(e){
     e.preventDefault();
     $('#myModal').modal('show').find('.modal-body').load($(this).attr('href'));
   });
</script>
   <script>
   $("#myModal").on('hidden.bs.modal', function () {
       $(this).data('bs.modal', null);
   });
   </script>
