<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Jadwal Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Jadwal</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Jadwal</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="add_schedule_proc">
              <div class="box-body">
                <div class="form-group">

                  <select name="id_kapal" class="form-control" id="id_kapal" required>
                    <option value="" selected>pilih boat</option>
                    <?php
                    $query=tampil_all_ref_boat();
                    while($row=mysql_fetch_array($query)){

                     ?>
                     <option value="<?php echo $row['id_boat'];?>"><?php echo $row['nama_boat'];?></option>


                     <?php }?>

                  </select>
                </div>
            <!-- keberangkatan -->
                <div class="bootstrap-timepicker">
                  <div class="form-group">


                    <div class='input-group date' id='datetimepicker3' data-date-format="HH:mm:ss"
       data-date-useseconds="true"
       data-date-pickDate="false" >
                        <input type='text' class="form-control" name="waktu_berangkat" placeholder="waktu keberangkatan" required/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>

                  </div>

                </div>


                <!-- kedatangan -->
                <div class="bootstrap-timepicker">
                  <div class="form-group">


                    <div class='input-group date' id='datetimepicker4' data-date-format="HH:mm:ss"
       data-date-useseconds="true"
       data-date-pickDate="false" >
                        <input type='text' class="form-control" name="waktu_datang" placeholder="waktu kedatangan" required/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>

                  </div>

                </div>

								<div class="form-group">


                    <select class="form-control" id="port_keberangkatan" name="port_berangkat" placeholder="Pelabuhan keberangkatan" required>
                      <option selected>Pelabuhan keberangkatan</option>
                      <?php
                      $qr=tampil_port();
                      while($rw=mysql_fetch_array($qr))
                      {
                        echo "<option value='$rw[nama_port]'>$rw[nama_port]</option>";
                      }
                      ?>
                    </select>
                </div>
								<div class="form-group">

                  <select class="form-control" id="port_kedatangan" name="port_datang" placeholder="Pelabuhan kedatangan" required>
                    <option selected>Pelabuhan kedatangan</option>
                    <?php
                    $qr=tampil_port();
                    while($rw=mysql_fetch_array($qr))
                    {
                      echo "<option value='$rw[nama_port]'>$rw[nama_port]</option>";
                    }
                    ?>
                  </select>


                </div>

                <div class="form-group">



                      <select name="jenis_jadwal" class="form-control" required>
                        <option value="" selected>Jenis Jadwal</option>
                        <option value="regular">Regular</option>
                        <option value="holiday">Holiday</option>
                        <option value="special">Special</option>
                      </select>

                </div>
                <div class="form-group">
                  <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" id="tarif" name="tarif_dewasa" placeholder="Tarif Dewasa" required>
              </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" id="tarif" name="tarif_anak" placeholder="Tarif Anak-anak" required>
              </div>
                </div>
              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambahkan</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>


<?php include"footer.php"; ?>
