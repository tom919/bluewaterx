<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Boat</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Boat</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
            $kode_boat=$_POST['kode_boat'];
            $nama_boat=$_POST['nama_boat'];
            $kapasitas=$_POST['kapasitas'];
            $ref_per=$_POST['ref_per'];

            $data="'$kode_boat','$nama_boat','$kapasitas','$ref_per'";
            $xst=tambah_boat($data);
            if($xst==10){

            echo "<div class='alert alert-success'>

                <h4><i class='icon fa fa-check'></i> Berhasil!</h4>
                Boat berhasil ditambahkan
                <a href='main_boat'><button class='btn btn-default pull-right'><i class='fa fa-arrow-left'></i>&nbsp;Kembali</button></a>
                <div class='clearfix'></div>
              </div>
            </div>";
            }else{

             echo "error level 1";

            }


            ?>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
