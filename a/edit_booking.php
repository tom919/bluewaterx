<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Booking
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Booking</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update Booking</h3>
            </div>
            <!-- /.box-header -->
            <?php
            $id=$_GET['id'];
            $q=pilih_booking($id);
            $row=mysql_fetch_array($q);

            ?>

            <!-- form start -->
            <form role="form" method="post" action="edit_booking_proc">
              <div class="box-body">
                <div class="form-group">
                  <label>Kode Booking</label>
                  <h3><?php echo $row['kode_booking']; ?></h3>
                  <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $row['id_booking']; ?>">

                </div>


                        <div class="form-group">


                      <label>Status</label>
                              <select name="status" class="form-control" required>
                                <option value="<?php echo $row['status'];?>" selected><?php echo $row['status'];?></option>
                                <option value="unconfirmed">Unconfirmed</option>
                                <option value="paid">Paid</option>
                                <option value="cancel">Cancel</option>
                              </select>

                        </div>


              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure?')"><i class="fa fa-pencil"></i>&nbsp;Update</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
