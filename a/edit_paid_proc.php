<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Booking
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Booking</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Konfirmasi Booking</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php

          $id=$_POST['id'];
          $status="paid";


?>
<!--- start confirm main system --->
<?php
$q=pilih_booking_tiket($id);
$row=mysql_fetch_array($q);
//kemudian kita membuat object library FPDF dan halaman
// P = orientasi jenis kertas, menggunakan Potrait
// mm = jenis ukuran milimeter
// A4 = ukuran kertas
$pdf = new PDF('P','mm',array(210,178));
$pdf->AddPage();

//salah satu yang paling wajib adalah kita harus menyertakan jenis huruf dan ukuran huruf dengan menggunakan fungsi SetFont()
//disini menggunakan jenis huruf Arial, ukuran huruf 16 dan tipe Bold
$pdf->SetFont('Arial','B',14);

$pdf->Image('../r/images/basic/logo3.png',10,10,-300);
//untuk menampilkan teks kedalam file PDF kita menggunakan fungsi Write()
//parameter awalnya untuk posisi teks 20, kemudian diikuti dengan isi teksnya "Hello World"
$pdf->Write('30','E-TICKET');
$pdf->Ln(20);
$pdf->WriteHTML('BOOKING ID  :'.$row['kode_booking'].'              UNIQUE CODE :'.$row['kode_unik']);

$pdf->Ln(10);
$pdf->Write('5','Customer Name :'.$row['nama_customer']);
$pdf->Ln(10);
$pdf->setFont('Arial','',8);
$pdf->text(120,10,'QR Verification');
//kemudian kita tambahkan lagi baris baru menggunakan fungsi Ln()
//dan kita tambahkan teks baru
//untuk posisinya biarkan saja 0 agar jarak spasinya tidak terlalu jauh

$pdf->Image($global_url.'/a/qrgenerator.php?idb='.$row['id_booking'].'&kdu='.$row['kode_unik'],120,10,0,20,'PNG');

$pdf->Ln(1);
$pdf->setFont('','',12);
$htmlcode='
<table>
      <tr>
      <td>Departure</td>
      <td>'.$row['port_keberangkatan'].'</td><td>At '.$row['jam_keberangkatan'].'</td>
      </tr>
      <tr>
      <td>Arrival</td>
      <td>'.$row['port_kedatangan'].'</td><td>At '.$row['jam_kedatangan'].'</td>
      </tr>

  </table>';

$pdf->WriteHTML($htmlcode);


$pdf->SetFont('Arial','B',14);
$pdf->Write('10','Passenger :');


$pdf->Ln(10);
$pdf->setFont('','',12);
$htmlcode2='
<table>
      <tr>
      <td>Name</td>
      <td>Gender</td><td>Age</td>
      </tr><tr>
          <td></td>
          <td></td><td></td>
          </tr>';

$pdf->WriteHTML($htmlcode2);
      $f=tampil_penumpang($id);
      while($fr=mysql_fetch_array($f)){
        $htmlcode3='
      <tr>
      <td>'.$fr['nama_penumpang'].'</td>
      <td>'.$fr['jenis_kelamin'].'</td>
      <td>'.$fr['umur'].'</td>
      </tr>';
      $pdf->WriteHTML($htmlcode3);
    }

$htmlcode4 ='</table>';
  $pdf->WriteHTML($htmlcode4);


//nah untuk fungsi Output() harus selalu digunakan, karena fungsi inilah yang akan mengenerate halaman PDF
// FOR EMAIL
$content = $pdf->Output('', 'S'); // Saving pdf to attach to email
$content = chunk_split(base64_encode($content));
// Email settings
$mailto = $row['email'];
$from_name = 'Bluewater Booking System';
$from_mail = $booking_email;
$replyto = 'no-reply';
$uid = md5(uniqid(time()));
$subject = 'Your E-TICKET'.$row['kode_booking'];
$message = '
<img src="'.$global_url.'/r/images/basic/logo3.png" width="200px" style="display:block">
<h3>Booking Payment</h3>
<br>
Dear '.$row['nama_customer'].',<br>
<br>
  Thankyou for using our services, here is your E-TICKET &nbsp;'.$row['kode_booking'].'
  <br><br>
  Best Regrads,
  <br><br>
  '.$site_name.'

';
$filename = $row['kode_booking'].'E-TICKET.pdf';
$header = "From: ".$from_name." <".$from_mail.">\r\n";
$header .= "Reply-To: ".$replyto."\r\n";
$header .= "MIME-Version: 1.0\r\n";
$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";

$header .= "This is a multi-part message in MIME format.\r\n";

$header .= "--".$uid."\r\n";
$header .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
//$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
$header .= $message."\r\n\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
$header .= "Content-Transfer-Encoding: base64\r\n";
$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
$header .= $content."\r\n\r\n";
$header .= "--".$uid."--";
$is_sent = mail($mailto, $subject, "", $header);
//$mpdf->Output(); // For sending Output to browser
//$pdf->Output('E-TICKET.pdf','D'); // For Download
?>
<!--- end of confirm main system --->
<?php


            $xst=edit_booking($id,$status);
            if($xst==10){

            echo "<div class='alert alert-success'>

                <h4><i class='icon fa fa-check'></i> Berhasil!</h4>
                Booking telah berhasil dikonfirmasi
                <a href='main_booking'><button class='btn btn-default pull-right'><i class='fa fa-arrow-left'></i>&nbsp;Kembali</button></a>
                <div class='clearfix'></div>
              </div>
            </div>";
            }else{

             echo "error level 1";

            }


            ?>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
