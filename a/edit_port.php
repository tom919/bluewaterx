<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Port
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Port</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Port</h3>
            </div>
            <!-- /.box-header -->
            <?php
            $id=$_GET['id'];
            $q=pilih_port($id);
            $row=mysql_fetch_array($q);

            ?>

            <!-- form start -->
            <form role="form" method="post" action="edit_port_proc">
              <div class="box-body">
                <div class="form-group">

                  <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $row['id_port']; ?>">

                </div>


                        <div class="form-group">
                          <label>Nama Port</label>
                          <div class="input-group">

                        <input type="text" class="form-control" id="nama_port" name="nama_port" value="<?php echo $row['nama_port']?>" required>
                      </div>
                        </div>
                        <div class="form-group">
                          <label>Wilayah</label>
                          <div class="input-group">
                        <input type="text" class="form-control" id="wilayah" name="wilayah" value="<?php echo $row['wilayah']?>" required>
                      </div>
                        </div>

              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
