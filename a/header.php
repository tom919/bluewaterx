<?php
ob_start();
session_start();
require('../r/pdf/html_table.php');
include"../r/login_controller.php";

include"../r/admin_controller.php";
include"../r/setting.php";

if(!$_SESSION['user_session'])

{

  echo"Please login";

  header('location:index');

}

$adminname=$_SESSION['user_name'];
$admindept=check_dept($adminname);
include"../r/setting.php";
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $site_name;?> | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="dist/dt/bootstrap-datetimepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">



  <!-- Page script -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="adminpage" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b><?php echo $site_name; ?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>&nbsp;<?php echo $site_name; ?></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- Notifications: style can be found in dropdown.less -->

          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $adminname;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $adminname;?> - <?php echo $admindept;?> departement
                  <small></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
              <!--  <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>-->
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
          <!--  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $adminname; ?> </p>
          <a href="#"><i class="fa fa-circle text-success"></i><b><?php echo $admindept;?></b></a>
        </div>
      </div>
      <!-- search form -->
  <!--    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <li class="active treeview">
          <a href="adminpage">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>

          </a>

        </li>

        <li>
          <a href="#">
            <i class="fa fa-fort-awesome"></i> <span>Port</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="main_port"><i class="fa fa-circle-o text-aqua"></i> Daftar Port</a></li>
            <li><a href="add_port"><i class="fa fa-circle-o text-red"></i> Tambah Port</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-briefcase"></i>
            <span>Perusahaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="add_company"><i class="fa fa-circle-o text-red"></i> Tambah Perusahaan</a></li>
            <li><a href="main_company"><i class="fa fa-circle-o text-aqua"></i> Daftar Perusahaan</a></li>

          </ul>

        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-ship"></i>
            <span>Boat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="add_boat"><i class="fa fa-circle-o text-red"></i> Tambah Boat</a></li>
            <li><a href="main_boat"><i class="fa fa-circle-o text-aqua"></i> Daftar Boat</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar"></i>
            <span>Jadwal</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="add_schedule"><i class="fa fa-circle-o text-red"></i> Tambah Jadwal</a></li>
            <li><a href="main_schedule"><i class="fa fa-circle-o text-aqua"></i> Daftar Jadwal</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar-check-o"></i> <span>Booking</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="main_booking"><i class="fa fa-circle-o text-aqua"></i> Daftar Booking</a></li>

          </ul>
        </li>

    <!--    <li class="treeview">
          <a href="#">
            <i class="fa fa-usd"></i> <span>Pembayaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> Pencarian</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-green"></i> Terbayar</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> Belum Terbayar</a></li>
          </ul>
        </li>-->
    <!--    <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> Tambah User</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> Daftar User</a></li>
          </ul>
        </li>-->
        <li>
          <a href="../b/wp-admin" target="_blank">
            <i class="fa fa-trophy"></i> <span>Blog</span>
            <!--<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>-->
          </a>
          <!--<ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> Tambah Promo</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> Daftar Promo</a></li>
          </ul>-->
        </li>

        <li>
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Slider</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="main_slider"><i class="fa fa-circle-o text-aqua"></i> Daftar Slider</a></li>
            <li><a href="add_slider"><i class="fa fa-circle-o text-red"></i> Tambah Slider</a></li>
          </ul>
        </li>

        <li><a href="profil"><i class="fa fa-cogs"></i> <span>Profil</span></a></li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
