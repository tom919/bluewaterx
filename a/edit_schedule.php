<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Jadwal</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Jadwal</h3>
            </div>
            <!-- /.box-header -->
            <?php
            $id=$_GET['id'];
            $q=pilih_jadwal($id);
            $row=mysql_fetch_array($q);

            ?>

            <!-- form start -->
            <form role="form" method="post" action="edit_schedule_proc">
              <div class="box-body">
                <div class="form-group">
                  <label>Kode Jadwal</label>
                  <h3><?php echo $row['id']; ?></h3>
                  <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $row['id']; ?>">

                </div>
                <div class="form-group">
                  <label>Nama Boat</label>
                  <select name="id_kapal" class="form-control" id="id_kapal" required>
                    <option value="<?php echo $row['id_kapal']?>" selected>
                    <?php
                    $z=tampil_ref_boat($row['id_kapal']);
                    $zr=mysql_fetch_array($z);
                    echo $zr['nama_boat'];
                    ?></option>
                    <?php
                    $que=tampil_all_ref_boat();
                    while($ro=mysql_fetch_array($que)){

                     ?>
                     <option value="<?php echo $ro['id_boat'];?>"><?php echo $ro['nama_boat'];?></option>


                     <?php }?>

                  </select>
                </div>
                <!-- keberangkatan -->
                    <div class="bootstrap-timepicker">
                      <div class="form-group">

                        <label>Waktu Keberangkatan</label>
                        <div class='input-group date' id='datetimepicker3' data-date-format="HH:mm:ss"
           data-date-useseconds="true"
           data-date-pickDate="false" >
                            <input type='text' class="form-control" name="waktu_berangkat" placeholder="waktu keberangkatan" value="<?php echo $row['keberangkatan']?>" required/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>

                      </div>

                    </div>


                    <!-- kedatangan -->
                        <div class="bootstrap-timepicker">
                          <div class="form-group">

                            <label>Waktu Kedatangan</label>
                            <div class='input-group date' id='datetimepicker4' data-date-format="HH:mm:ss"
               data-date-useseconds="true"
               data-date-pickDate="false" >
                                <input type='text' class="form-control" name="waktu_datang" placeholder="waktu kedatangan" value="<?php echo $row['kedatangan']?>" required/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>

                          </div>

                        </div>



                        <div class="form-group">
                      <label>Pelabuhan Keberangkatan</label>

                            <select class="form-control" id="port_keberangkatan" name="port_berangkat" placeholder="Pelabuhan keberangkatan" required>
                              <option value="<?php echo $row['port_keberangkatan'];?>" selected><?php echo $row['port_keberangkatan'];?></option>
                              <?php
                              $qr=tampil_port();
                              while($rw=mysql_fetch_array($qr))
                              {
                                echo "<option value='$rw[nama_port]'>$rw[nama_port]</option>";
                              }
                              ?>
                            </select>
                        </div>
                        <div class="form-group">
                      <label>Pelabuhan Kedatangan</label>

                        <select class="form-control" id="port_kedatangan" name="port_datang" placeholder="Pelabuhan kedatangan" required>
                          <option value="<?php echo $row['port_kedatangan'];?>" selected><?php echo $row['port_kedatangan'];?></option>
                          <?php
                          $qr=tampil_port();
                          while($rw=mysql_fetch_array($qr))
                          {
                            echo "<option value='$rw[nama_port]'>$rw[nama_port]</option>";
                          }
                          ?>
                        </select>

                        </div>

                        <div class="form-group">


                      <label>Jenis Jadwal</label>
                              <select name="jenis_jadwal" class="form-control" required>
                                <option value="<?php echo $row['jenis'];?>" selected><?php echo $row['jenis'];?></option>
                                <option value="regular">Regular</option>
                                <option value="holiday">Holiday</option>
                                <option value="special">Special</option>
                              </select>

                        </div>
                        <div class="form-group">
                          <label>Tarif dewasa</label>
                          <div class="input-group">
                          <span class="input-group-addon">Rp.</span>
                        <input type="number" class="form-control" id="tarif" name="tarif_dewasa" placeholder="Tarif Dewasa" value="<?php echo $row['tarif_dewasa']?>" required>
                      </div>
                        </div>
                        <div class="form-group">
                          <label>Tarif anak-anak</label>
                          <div class="input-group">
                          <span class="input-group-addon">Rp.</span>
                        <input type="number" class="form-control" id="tarif" name="tarif_anak" placeholder="Tarif Anak-anak" value="<?php echo $row['tarif_anak']?>" required>
                      </div>
                        </div>

              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
