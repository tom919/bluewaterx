<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Slider
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Slider</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Slider</h3>
            </div>
            <!-- /.box-header -->
            <?php
            $id=$_GET['id'];
            $q=pilih_slider($id);
            $row=mysql_fetch_array($q);

            ?>

            <!-- form start -->
            <form role="form" method="post" action="edit_slide_proc" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label>Id Slide</label>
                  <h3><?php echo $row['id']; ?></h3>
                  <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $row['id']; ?>" required>
                  <input type="hidden" class="form-control" id="fotoid" name="fotoid" value="<?php echo $row['foto']; ?>" required>
                </div>
                <div class="form-group">
                  <label>Ukuran Slide 1616x766 px, Max 1 MB</label>
                   <img src="../r/images/slider/<?php echo $row['foto']; ?>.jpg" style="max-width: 35%;">
                  <input type="file" class="form-control" id="foto" name="foto">
                </div>

								<div class="form-group">
                  <label>Caption</label>
									<input type="text" class="form-control" id="caption" name="caption" value="<?php echo $row['caption']; ?>" required>
								</div>
								<div class="form-group">
                  <label>Content</label>
									<input type="text" class="form-control" id="content" name="content" value="<?php echo $row['content']; ?>" required>
								</div>
                <div class="form-group">
                  <label>Link title</label>
                  <input type="text" class="form-control" id="link_title" name="link_title" value="<?php echo $row['link_title']; ?>" required>
                </div>
                <div class="form-group">
                <label>contoh : http://linkaddress.com</label>
                  <input type="text" class="form-control" id="link" name="link" value="<?php echo $row['link']; ?>" required>
                </div>
              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
