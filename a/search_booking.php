<?php include"header.php";?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Booking
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Cari Booking</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
        <div class="col-md-8">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Pencarian Booking</h3>



              <div class="box-tools pull-right">


              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php

                                $batas = 10;
                  $pg = isset( $_GET['pg'] ) ? $_GET['pg'] : "";

                  if ( empty( $pg ) ) {
                  $posisi = 0;
                  $pg = 1;
                  } else {
                  $posisi = ( $pg - 1 ) * $batas;
                  }



                  $term=$_GET['term'];
                  $fields=$_GET['fields'];

              $query=cari_booking_general($term,$fields,$posisi,$batas);
              echo "Hasil pencarian untuk :&nbsp;".$term;
              ?>

              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Booking Id</th>

                    <th>Nama customer</th>
                    <th>Tanggal Berangkat</th>
                    <th>Status</th>
                    <th>Action</th>

                  </tr>
                  </thead>
                  <tbody>
                    <?php  while($row=mysql_fetch_array($query)){ ?>
                  <tr>
                    <td><?php echo $row['kode_booking'];?></td>

                    <td>
                    <?php echo $row['nama_customer'];?>

                    </td>
                    <td><?php echo $row['tanggal_berangkat']; ?></td>
                    <td><?php echo $row['status'];?></td>

                    <td>
                      <div class="btn-group">
                           <button type="button" class="btn btn-info">Action</button>
                           <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                             <span class="caret"></span>
                             <span class="sr-only">Toggle Dropdown</span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                             <li><a href="detail_booking?id=<?php echo $row['id_booking']?>" data-toggle="modal" data-target="#myModal">Detail</a></li>
                              <li><a href="edit_paid?id=<?php echo $row['id_booking']?>">Konfirm Bayar</a></li>
                             <li><a href="edit_booking?id=<?php echo $row['id_booking']?>">Update Booking</a></li>


                             <li><a href="delete_booking?id=<?php echo $row['id_booking']?>&idc=<?php echo $row['id_customer']?>">Hapus</a></li>
                           </ul>
                         </div>
                    </td>
                  </tr>
                  <?php }?>

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <p class="pull-right" ><?php
              $jml_data=jumlah_booking();





              //Jumlah halaman
$JmlHalaman = ceil($jml_data/$batas); //ceil digunakan untuk pembulatan keatas

//Navigasi ke sebelumnya
if ( $pg > 1 ) {
$link = $pg-1;
$prev = "<a href='search_booking?pg=$link&term=$term'>Sebelumnya </a>";
} else {
$prev = "Sebelumnya ";
}

//Navigasi nomor
$nmr = '';
for ( $i = 1; $i<= $JmlHalaman; $i++ ){

if ( $i == $pg ) {
$nmr .= $i;
} else {
$nmr .= "|<a href='search_booking?pg=$i&term=$term'>$i</a> ";
}
}

//Navigasi ke selanjutnya
if ( $pg < $JmlHalaman ) {
$link = $pg + 1;
$next = " <a href='search_booking?pg=$link&term=$term'>Selanjutnya</a>";
} else {
$next = " Selanjutnya";
}

//Tampilkan navigasi

echo"<div class='pull-left'>
   $prev |

   $nmr |

   $next
   </div>
 ";


?>
              </p>
            </div>
            <!-- /.box-footer -->
          </div>
        </div>






        <div class="col-md-3">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Pencarian</h3>



              <div class="box-tools pull-right">

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">



                <div class="box-tools margin">
                  <?php

                    modul_cari_booking();
                  ?>
                </div>


                <div class="box-tools margin">
                  <div class="input-group input-group-sm" style="width: 150px;">


                    <div class="input-group-btn">

                    </div>
                  </div>
                </div>



              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
          </div>



          <!-- Modal -->
           <div class="modal fade" id="myModal" role="dialog">
             <div class="modal-dialog">

               <!-- Modal content-->
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                   <h4 class="modal-title">Detail Booking</h4>
                 </div>
                 <div class="modal-body">

                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 </div>
               </div>

             </div>

           </div>




      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
