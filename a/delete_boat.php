<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Boat</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">



            <!-- /.box-header -->
            <div class='alert alert-warning'>
<?php
  $kode_boat=$_GET['id'];
?>
                        <h4><i class='icon fa fa-warning'></i> Konfirmasi Penghapusan</h4>
                        Anda yakin akan menghapus Boat dengan Kode <?php echo $kode_boat ?> &nbsp;
                        <div>
                          <br>
                          <p class="pull-right">
                        <a href='delete_boat_proc?id=<?php echo $kode_boat?>'><button type='button'  class='btn btn-danger'><i class='fa fa-trash-o'></i>&nbsp;Hapus</button></a>
                       &nbsp;&nbsp;&nbsp;

                        <a href='main_boat'><button type='button' class='btn btn-success'><i class='fa fa-ban'></i>&nbsp;Batal</button></a>
                      </p>
                      </div>

                        <div class='clearfix'></div>
                      </div>
                    </div>";
          </div>

<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
