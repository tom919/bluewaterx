<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-controller="DBController">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Port
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Port</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Port</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="add_port_proc">
              <div class="box-body">

                <div class="form-group">
                  <div class="input-group">

                <input type="text" class="form-control" id="nama_port" name="nama_port" placeholder="Nama Port" required>
              </div>
                </div>

                <div class="form-group">
                  <div class="input-group">

                <input type="text" class="form-control" id="wilayah" name="wilayah" placeholder="Wilayah" required>
              </div>
                </div>
              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambahkan</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>


<?php include"footer.php"; ?>
