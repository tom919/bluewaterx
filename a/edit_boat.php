<?php include"header.php";?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Boat
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Boat</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
				<div class="col-md-3"></div>

				<div class="col-md-6">

				<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Boat</h3>
            </div>
            <!-- /.box-header -->
            <?php
            $id=$_GET['id'];
            $q=pilih_boat($id);
            $row=mysql_fetch_array($q);

            ?>

            <!-- form start -->
            <form role="form" method="post" action="edit_boat_proc">
              <div class="box-body">
                <div class="form-group">
                  <label>Kode Boat</label>
                  <h3><?php echo $row['id_boat']; ?></h3>
                  <input type="hidden" class="form-control" id="kode_boat" name="kode_boat" value="<?php echo $row['id_boat']; ?>" required>

                </div>
                <div class="form-group">
                  <label>Nama Boat</label>
                  <input type="text" class="form-control" id="nama_boat" name="nama_boat" value="<?php echo $row['nama_boat']; ?>" required>
                </div>
								<div class="form-group">
                  <label>Kapasitas</label>
									<input type="number" class="form-control" id="kapasitas" name="kapasitas" value="<?php echo $row['kapasitas']; ?>" required>
								</div>
								<div class="form-group">
                  <label>Perusahaan pemilik</label>
                  <select name="ref_per" class="form-control" id="ref_per" required>
                    <option value="<?php echo $row['ref_per']?>" selected>
                      <?php $term=$row['ref_per'];
                      $q=cari_ref_per($term);
                      $r=mysql_fetch_array($q);
                      echo $r['nama_perusahaan'];
                       ?>

                    </option>
                    <?php
                    $query=referensi_per();
                    while($row=mysql_fetch_array($query)){

                     ?>
                     <option value="<?php echo $row['id_per'];?>"><?php echo $row['nama_perusahaan'];?></option>


                     <?php }?>
                   </select>
								</div>


              <!-- /.box-body -->

              <div class="box-footer pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;Update</button>
              </div>
            </form>
          </div>
				</div>
<div class="col-md-3"></div>

      <div class="clearfix"></div>

      </section>
  </div>

<?php include"footer.php"; ?>
